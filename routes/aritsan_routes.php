<?php 
//migrate/install
Route::get('/migrate/{key}',  array('as' => 'install', function($key = null)
{
    

}));

//migrate/refresh
Route::get('/migrate/{key}',  array('as' => 'refresh', function($key = null)
{

    if($key == "install"){
        try {
            echo '<br>init migrate:install...';
            Artisan::call('migrate');
            echo 'done migrate:install';

        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }

    if($key == "refresh"){
        try {
            echo '<br>init migrate:refresh...';
            Artisan::call('migrate:refresh');
            echo 'done migrate:refresh';

        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }

}));


//Setup route example db/seed
Route::get('/db/{key}',  array('as' => 'install', function($key = null)
{
    if($key == "seed"){
        try {

            echo '<br>init tables seader...';
            Artisan::call('db:seed');
            echo '<br>done with seader';
        } catch (ReflectionException $e) {
            Response::make($e->getMessage(), 500);
        }
    }else{
        App::abort(404);
    }

}));

Route::get('log',function ()
{
    // from PHP documentations
     $logFile = file(storage_path().'/logs/laravel.log');
     $logCollection = [];
     // Loop through an array, show HTML source as HTML source; and line numbers too.
     foreach ($logFile as $line_num => $line) {
        $logCollection[] = array('line'=> $line_num, 'content'=> htmlspecialchars($line));
     }
     dump($logFile);
});

Route::get('composer',function ()
{
    // from PHP documentations
    system('composer dump-autoload');
   // dump($result); 

}); 