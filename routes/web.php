<?php

include __DIR__ . '/aritsan_routes.php';
Route::get('test',function (){
    return view('vendor\notifications\email');
    $order = \App\Models\Order::whereNotNull('trip_id')->first();
//    dd($order);
    return $order->childrenNamePriceTrip();
});
Route::get('logs', 'LogViewerController@index');

// Authentication Routes...
$this->get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('admin/login', 'Auth\LoginController@login')->name('do-login');
$this->get('admin/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/request', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

/*
|--------------------------------------------------------------------------
| Admin Protected Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes for admin role
|	, 'middleware' => ['auth', 'role:admin']
*/
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'role:admin|staff']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'role:admin']], function () {

    // user managmet
    Route::resource('users', 'AdminController');
    Route::resource('pages', 'PageController');

    // settigns routes group
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/site_settings', 'SettingController@siteSetting')->name('site');
        Route::post('/site_settings', 'SettingController@storeSiteSetting')->name('store.site');

        Route::get('general', 'SettingController@showGeneral')->name('general');
        Route::post('general', 'SettingController@updateGeneral')->name('general');

        Route::get('mail', 'SettingController@showMail')->name('mail');
        Route::post('mail', 'SettingController@updateMail')->name('mail');

        Route::post('send-email', 'SettingController@sendEmail')->name('send-email');

        Route::get('gateway', 'SettingController@showGateway')->name('gateway');
        Route::get('admin-gateway', 'SettingController@showGatewayAdmin')->name('gateway-admin');
        Route::post('gateway', 'SettingController@updateGateway')->name('gateway');

        Route::delete('{setting_id}/email/{email?}', [
            'as' => 'destroy_email',
            'uses' => 'SettingController@destroy_email'
        ]);

        Route::get('/twilio', 'SettingController@twilioSetting')->name('twilio');
        Route::post('/twilio', 'SettingController@storeTwilioSetting')->name('store.twilio');
        
    });

});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::group(['as'=>'admin.'], function () {
        Route::resource('home', 'HomePageController');
    });
});


/*
|--------------------------------------------------------------------------
| AuthUsers Protected Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes for authenticated Users
|
*/

Route::group(['namespace' => 'AuthUsers', 'middleware' => ['auth']], function () {

    Route::get('index', 'SampleController@index')->name('index');

});


/*
|--------------------------------------------------------------------------
| App Guest Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes unauthenticated users
|
*/

Route::group(['namespace' => 'App'], function () {


});


Route::get('log', function () {
    // from PHP documentations
    $logFile = file(storage_path() . '/logs/laravel.log');
    $logCollection = [];
    // Loop through an array, show HTML source as HTML source; and line numbers too.
    foreach ($logFile as $line_num => $line) {
        $logCollection[] = array('line' => $line_num, 'content' => htmlspecialchars($line));
    }
    dump($logFile);
});

Route::get('log/delete', function () {

    $file = storage_path() . '/logs/laravel.log';
    if (unlink($file)) {
        dump('deleted');
    }


});

Route::get('/home', 'FrontendController@homepage')->name('home');

//-------------------- Email Template Module -------------------//
Route::resource('email_templates','EmailTemplateController');
//-------------------- End ------------------------------------//
