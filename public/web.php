<?php



/*
|--------------------------------------------------------------------------
| Admin Protected Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes for admin role
|	, 'middleware' => ['auth', 'role:admin']
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'role:admin']], function() {

	// user managmet 
	Route::get('dashboard', 'DashboardController@index')->name('dashboard');;
	Route::resource('users', 'AdminController');


	// settigns routes group
	Route::group(['prefix' => 'settings', 'as' => 'settings.'], function ()
	{	
	 	Route::get('general', 'SettingController@showGeneral')->name('general');
	 	Route::post('general', 'SettingController@updateGeneral')->name('general');

	 	Route::get('mail', 'SettingController@showMail')->name('mail');
	 	Route::post('mail', 'SettingController@updateMail')->name('mail');

	 	Route::post('send-email', 'SettingController@sendEmail')->name('send-email');
                
	 	Route::get('gateway', 'SettingController@showGateway')->name('gateway');
	 	Route::post('gateway', 'SettingController@updateGateway')->name('gateway');

	 	Route::delete('{setting_id}/email/{email?}', [
	 		'as' => 'destroy_email',
	 		'uses' => 'SettingController@destroy_email'
	 	]);
	});

}); 

/*
|--------------------------------------------------------------------------
| AuthUsers Protected Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes for authenticated Users
|
*/

Route::group(['namespace' => 'AuthUsers', 'middleware' => ['auth']], function() {

	 Route::get('index', 'SampleController@index')->name('index');

	

});

/*
|--------------------------------------------------------------------------
| App Guest Routes
|--------------------------------------------------------------------------
|
| Here you can register web routes unauthenticated users
|
*/

Route::group(['namespace' => 'App'], function() {

	 Route::get('/', 'HomeController@index')->name('home');

});


