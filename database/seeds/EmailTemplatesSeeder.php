<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_templates')->truncate();

        DB::table('email_templates')->insert([
            [
                'title' => 'Failed Transaction',
                'subject' => 'Failed Transaction',
                'static_body' => 'Dear [member],

Unfortunately your card was declined when attempting to make your membership purchase for the Snowflake Club.  This could mean that you have a typo in your address and therefore the credit card processor rejects the order or any number of other reasons.  You will need to return to the Snowflake site and start your membership purchase again. 

Please call us at [phone] if you have any questions or need assistance.',

                'dynamic_body' => '<p>Failed Transaction</p>',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],

            [
                
                'title' => 'Refund Issued',
                'subject' => 'Refund Issued',
                'static_body' => "Dear [Customer_Name]

[site_name] has issued you a refund in the amount of [Refund_Amount] for your order.

If you have any questions about this refund, don't hesitate to contact our offices at [site_phone].
",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                
                'title' => 'Refund for Epic Pass',
                'subject' => 'Refund for Epic Pass',
                'static_body' => "Dear [Customer_Name]

[site_name] has issued you a refund for Epic Pass in the amount of [Refund_Amount] for your order.

If you have any questions about this refund, don't hesitate to contact our offices at [site_phone].

",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                
                'title' => 'Refund Issued',
                'subject' => 'Cancellation Refund',
                'static_body' => "Dear [Customer_Name]

[site_name] has issued you a Cancellation Refund in the amount of [Refund_Amount] for your order.

If you have any questions about this refund, don't hesitate to contact our offices at [site_phone].

",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                
                'title' => 'Additional Charge',
                'subject' => 'Charge for Parent Bus Fee',
                'static_body' => "Dear [name],
                
We have paid the balance [amount_charging] on your order for Parent Bus Fee with [site_name] using credit card ending in [customer_card].

Thank you for selecting [site_name].",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                
                'title' => 'Additional Charge',
                'subject' => 'Charge for Lift Ticket',
                'static_body' => "Dear [name],

We have paid the balance [amount_charging] on your order for Lift Ticket  with [site_name] using credit card ending in [customer_card].

Thank you for selecting [site_name].",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                
                'title' => 'Additional Charge',
                'subject' => 'Charge for Equipment Rental',
                'static_body' => "Dear [name],

We have paid the balance [amount_charging] on your order for Equipment Rental with [site_name] using credit card ending in [customer_card].

Thank you for selecting [site_name].",
                'dynamic_body' => '',
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            /*end refund and charge*/
            
        ]);
    }
}
