<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedValidationsToSettingsTable extends Migration {

	protected function addValidations($key, array $rules)
	{
		\DB::table('settings')
			->where('key', $key)
			->update(['validation_rules' => json_encode($rules)]);
	}

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$this->addValidations('general', [
			'config.site_name' => 'required',
			'config.site_url' => 'required',
			'config.default_email' => 'required|email',
			'config.reservation_fee' => 'required|numeric|min:0',
			'config.discount' => 'required|numeric|min:0',
			'config.in_advance_rental_days' => 'required|numeric|min:0',
			'config.season_start' => 'required|date_format:m/d/Y',
			'config.season_end' => 'required|date_format:m/d/Y'
		]);

		$this->addValidations('limitation', [
			'config.age' => 'required|numeric',
			'config.height' => 'required',
			'config.weight' => 'required',
			'config.shoe_size' => 'required'
		]);

		$this->addValidations('gateway', [
			'config.is_live' => 'required|in:0,1',
			'config.login_id' => 'required',
			'config.transaction_key' => 'required'
		]);

		$this->addValidations('mail', [
			'config.host' => 'required',
			'config.port' => 'required|numeric',
			'config.username' => 'required',
			'config.password' => 'required',
			'config.from.name' => 'required',
			'config.from.address' => 'required'
		]);

		$this->addValidations('backup', [
			'config.selected' => 'required|in:daily,bi-weekly,weekly,bi-monthly,monthly'
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
