<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedGatewaySettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::table('settings')->insert([
			'key' => 'gateway',
			'view_name' => 'settings.gateway',
			'title' => 'Gateway Settings',
			'config' => '{"is_live":0,"login_id":"","transaction_key":""}'
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::table('settings')
			->where('key', 'gateway')
			->delete();
	}

}
