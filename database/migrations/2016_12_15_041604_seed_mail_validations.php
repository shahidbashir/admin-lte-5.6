<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedMailValidations extends Migration {

    protected $setting = 'mail';

    protected function getDecodedConfig()
    {
        $config = \DB::table('settings')
            ->select('config')
            ->where('key', $this->setting)
            ->first()
            ->config;

        return json_decode($config, true);
    }

    protected function getDecodedValidationRules()
    {
        $validationRules = \DB::table('settings')
            ->select('validation_rules')
            ->where('key', $this->setting)
            ->first()
            ->validation_rules;

        return json_decode($validationRules, true);
    }

    protected function encodeAndPersistConfig(array $config)
    {
        \DB::table('settings')
            ->where('key', $this->setting)
            ->update(['config' => json_encode($config)]);
    }

    protected function encodeAndPersistValidationRules(array $validationRules)
    {
        \DB::table('settings')
            ->where('key', $this->setting)
            ->update(['validation_rules' => json_encode($validationRules)]);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = $this->getDecodedConfig();
        $validationRules = $this->getDecodedValidationRules();

        $validationRules['config.driver']   = 'required';

        $validationRules['config.host']     = 'required_if:config.driver,smtp';
        $validationRules['config.port']     = 'required_if:config.driver,smtp|numeric';
        $validationRules['config.username'] = 'required_if:config.driver,smtp';
        $validationRules['config.password'] = 'required_if:config.driver,smtp';

        $validationRules['config.mailgun_domain']   = 'required_if:config.driver,mailgun';
        $validationRules['config.mailgun_key']      = 'required_if:config.driver,mailgun';

        $this->encodeAndPersistConfig($config);
        $this->encodeAndPersistValidationRules($validationRules);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $config = $this->getDecodedConfig();
        $validationRules = $this->getDecodedValidationRules();

        unset($validationRules['config.driver']);

        unset($validationRules['config.host']);
        unset($validationRules['config.port']);
        unset($validationRules['config.username']);
        unset($validationRules['config.password']);

        unset($validationRules['config.mailgun_domain']);
        unset($validationRules['config.mailgun_key']);

        $this->encodeAndPersistConfig($config);
        $this->encodeAndPersistValidationRules($validationRules);
    }

}
