<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMailgunDomainAndMailgunKeyFields extends Migration {

    protected $setting = 'mail';

    protected function getDecodedConfig()
    {
        $config = \DB::table('settings')
            ->select('config')
            ->where('key', $this->setting)
            ->first()
            ->config;

        return json_decode($config, true);
    }

    protected function getDecodedValidationRules()
    {
        $validationRules = \DB::table('settings')
            ->select('validation_rules')
            ->where('key', $this->setting)
            ->first()
            ->validation_rules;

        return json_decode($validationRules, true);
    }

    protected function encodeAndPersistConfig(array $config)
    {
        \DB::table('settings')
            ->where('key', $this->setting)
            ->update(['config' => json_encode($config)]);
    }

    protected function encodeAndPersistValidationRules(array $validationRules)
    {
        \DB::table('settings')
            ->where('key', $this->setting)
            ->update(['validation_rules' => json_encode($validationRules)]);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = $this->getDecodedConfig();
        $validationRules = $this->getDecodedValidationRules();

        if(isset($config['mailgun_domain'])){
            $config['domain'] = $config['mailgun_domain'];
            unset($config['mailgun_domain']);
        } else {
            $config['domain'] = '';
        }

        if(isset($validationRules['config.mailgun_domain'])){
            $validationRules['config.domain'] = $validationRules['config.mailgun_domain'];
            unset($validationRules['config.mailgun_domain']);
        } else {
            $validationRules['config.domain'] = 'required_if:config.driver,mailgun';
        }

        if(isset($config['mailgun_key'])){
            $config['secret'] = $config['mailgun_key'];
            unset($config['mailgun_key']);
        } else {
            $config['mailgun_key'] = '';
        }

        if(isset($validationRules['config.mailgun_key'])){
            $validationRules['config.secret'] = $validationRules['config.mailgun_key'];
            unset($validationRules['config.mailgun_key']);
        } else {
            $validationRules['config.mailgun_key'] = 'required_if:config.driver,mailgun';
        }

        $this->encodeAndPersistConfig($config);
        $this->encodeAndPersistValidationRules($validationRules);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $config = $this->getDecodedConfig();
        $validationRules = $this->getDecodedValidationRules();

        $config['mailgun_domain'] = $config['domain'];
        unset($config['domain']);

        $validationRules['config.mailgun_domain'] = $validationRules['config.domain'];
        unset($validationRules['config.domain']);

        $config['mailgun_key'] = $config['secret'];
        unset($config['secret']);

        $validationRules['config.mailgun_key'] = $validationRules['config.secret'];
        unset($validationRules['config.secret']);

        $this->encodeAndPersistConfig($config);
        $this->encodeAndPersistValidationRules($validationRules);
    }
}
