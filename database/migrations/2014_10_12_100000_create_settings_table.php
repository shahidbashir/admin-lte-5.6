<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id');

			//   the key which will be used to
			// refer to this specific setting
			$table->string('key')->unique();

			// name of the view which must be rendered for the setting
			$table->string('view_name');

			// title and description for setting
			$table->string('title');
			$table->string('description')->nullable();

			// key => value pairs
			$table->text('config')->nullable();

			// key => value validation_rules
			$table->text('validation_rules')->nullable();

			

			$table->nullableTimestamps();
		});

		\DB::table('settings')->insert([
			[
				'key' => 'general',
				'view_name' => 'settings.general',
				'title' => 'General Settings',
				'config' => '{"site_name":"","default_email":"","site_url":"","additional_emails":[],"in_advance_rental_days":1,"reservation_fee":10,"discount":0}'
			],
			[
				'key' => 'limitation',
				'view_name' => 'settings.limitation',
				'title' => 'Limitation Settings',
				'config' => '{"age":"14","height":"5\' 5\'\'","weight":"150 lbs","shoe_size":7.5}',
			],
			[
				'key' => 'mail',
				'view_name' => 'settings.mail',
				'title' => 'Mail Settings',
				'config' => '{"host":"","port":"","username":"","password":"","from":{"name":"","address":""}}',
			]
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
