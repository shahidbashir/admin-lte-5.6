<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="publishable-key" content="{{ config('gateway.stripe.publish') }}">


    <title>@yield('title') | {{ config('general.site_name', 'Laravel Systems') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{assetsUrl()}}/plugins/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{assetsUrl()}}/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{assetsUrl()}}/plugins/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{assetsUrl()}}/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{assetsUrl()}}/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="{{assetsUrl()}}/plugins/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="{{assetsUrl()}}/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">

    <link href="{{ url('assets/css/style.css') }}" rel="stylesheet">

@yield('css')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
    input[type='number'] {
        -moz-appearance: textfield;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }

</style>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<body class="hold-transition skin-blue ">
<!-- Site wrapper -->
<div class="wrapper">

@include('layouts.partials.header')

<!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
@include('layouts.partials.left')

<!-- =============================================== -->

    @yield('content')

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>System Date and Time</b> <?php echo  date('m/d/y h:i a e') ?>
        </div>
        <strong>Copyright &copy; {{ date("Y") }} <a
                    href="{{ url('/') }}">{{ config('general.site_name', 'Laravel Systems') }}</a>.</strong> All rights
        reserved.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{assetsUrl()}}/plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{assetsUrl()}}/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="{{assetsUrl()}}/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{assetsUrl()}}/plugins/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{assetsUrl()}}/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{assetsUrl()}}/js/demo.js"></script>

<!-- Sweet Alert -->
<script src="{{assetsUrl()}}/plugins/sweetalert/sweetalert.min.js"></script>


<?php if (true) { ?>
<script src="{{assetsUrl()}}/plugins/moment/moment.js"></script>
<script src="{{assetsUrl()}}/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="{{assetsUrl()}}/plugins/bootstrap-daterangepicker/daterangepicker.css">

<script src="{{assetsUrl()}}/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="{{assetsUrl()}}/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="{{assetsUrl()}}/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="{{assetsUrl()}}/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<script src="{{assetsUrl()}}/plugins/select2/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{assetsUrl()}}/plugins/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="{{assetsUrl()}}/css/alt/AdminLTE-select2.min.css">


<script src="{{assetsUrl()}}/plugins/tiny_mce/tinymce.min.js"></script>

<script type="text/javascript">
    tinymce.init({
        theme: "modern",
        mode: "specific_textareas",
        editor_selector: "mceEditor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools moxiemanager",
            "insertdatetime media table contextmenu jbimages"
        ],

        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,
        document_base_url: "{{url('/')}}",

        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });

    var g_readTerms = false;


</script>


<?php } ?>

<script src="{{assetsUrl()}}/js/main.js"></script>
<script src="{{asset('/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(window).on('load', function()
    {
        phonemark();
        datepick();
        timepick();
        date_of_birth()
    });

    function phonemark() {
        var phones = [{"mask": "(###) ###-####"}, {"mask": "(###) ###-####"}];
        $('.phonebox').inputmask({
            mask: phones,
            greedy: false,
            definitions: {'#': {validator: "[0-9]", cardinality: 1}}
        });
    }
    function datepick() {
        if ($('.singledatepicker').length) {

            $('.singledatepicker').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true

            });

        };
    }
    function timepick() {
            $(function () {
                $('.timepick').timepicker({
//                    format: 'LT'
                    defaultTime:false
                });
            });
    }
    function date_of_birth() {

        var date_of_birth = [{ "mask": "##/##/####"},{ "placeholder": "mm/dd/yyyy" }];
        $('#date_of_birth').inputmask({
            mask: date_of_birth,
            greedy: false,
            definitions: { '#': { validator: "[0-9]", cardinality: 1}}
        });
        $('.mask_dob').inputmask({
            mask: date_of_birth,
            greedy: false,
            definitions: { '#': { validator: "[0-9]", cardinality: 1}}
        });

    }
</script>


@yield('javascript')

<script>
    @if(Session::has('success'))
        flashMessage = {
        type: "success",
        description: "{{ Session::pull('success') }}"
    };
    @elseif(Session::has('info'))
        flashMessage = {
        type: "info",
        description: "{{ Session::pull('info') }}"
    };
    @elseif(Session::has('warning'))
        flashMessage = {
        type: "warning",
        description: "{{ Session::pull('warning') }}"
    };
    @elseif(Session::has('error'))
        flashMessage = {
        type: "error",
        description: "{{ Session::pull('error') }}"
    };
    @endif

    if (typeof flashMessage !== 'undefined') {
        GLOBAL.displayFlashMessage(flashMessage);
    }
</script>


</body>
</html>


<style>
    .content {
        min-height: 250px;
        padding: 15px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 25px;
        padding-right: 25px;
        padding-top: 25px;
        padding-bottom: 100px;
    }

    .table-hover {
        border: 1px solid #ddd;
    }

    .table-hover > thead > tr > th,
    .table-hover > tbody > tr > th,
    .table-hover > tfoot > tr > th,
    .table-hover > thead > tr > td,
    .table-hover > tbody > tr > td,
    .table-hover > tfoot > tr > td {
        border: 1px solid #ddd;
    }

    .table-hover > thead > tr > th,
    .table-hover > thead > tr > td {
        border-bottom-width: 2px;
        vertical-align: middle;;
    }

    .table > thead:first-child > tr:first-child > th {
        border-top: 0;
        border-bottom: 0;
    }
    
     td.text-center  a i.fa {
            margin-right: 10px;
        }

</style>
