<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{assetsUrl()}}/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p> {{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Static Pages</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @foreach (App\Models\Page::all() as $record )
                        <li><a href="{{url('admin')}}/pages/{{$record->id}}/edit">{{$record->title}}</a></li>
                    @endforeach

                </ul>
            </li>
            @if(!isStaff())
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.users.index')}}"><i class="fa fa-users"></i>Admins</a></li>
                    <li><a href="{{ route('admin.settings.general')}}"><i class="fa fa-globe"></i>General</a></li>
                    <li><a href="{{ route('admin.settings.mail')}}"><i class="fa fa-envelope-o"></i>Mail</a></li>
                    <li>
                        <a href="{{ route('admin.settings.site') }}"><i class="fa  fa-sitemap"></i>Site Settings</a>
                    </li>
                    <li><a href="{{ route('email_templates.index') }}"><i class="fa fa-users"></i>Email Templates</a></li>
                    <li>
                        <a href="{{ route('admin.settings.twilio') }}"><i class="fa fa-comment-o"></i>Twilio
                            Settings</a>
                    </li>
                    <li><a href="{{ route('admin.settings.gateway')}}"><i class="fa fa-credit-card"></i>Gateway</a>
                    </li>
                </ul>
            </li>
                @endif


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>