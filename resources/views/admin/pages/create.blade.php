@extends('layouts.default')

@section('title')
	Add Page
	@parent
@stop

@section('content')

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Main content -->
		<section class="content">

			<div class="row">
				<div class="col-lg-12 col-md-12">

					<div class="box">
						<div class="box-header">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<h3 class="box-title">Add Page</h3>
									<p style="margin-top: 10px;">
										<a href="{{ route('admin.pages.index')}}" class="btn btn-success">Back to list</a>
									</p>
								</div>
							</div>
						</div><!-- .box-header ENDs -->


						<div class="box-body">
							@include('shared.errors')
							<div class="row">
								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 ">

									<form action="{{ route('admin.pages.store')}}" method="POST" enctype="multipart/form-data">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="form-group" style="margin-left: 0;">
										  <label for="title">Title:</label>
										  <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
										</div>

										<div class="form-group" style="margin-left: 0;">
											<label for="content">Content:</label>
											<textarea name="content" rows="9" cols="80" class="mceEditor" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('content') }}</textarea>
										</div>

										<div class="form-group">
										  <label for="pdf">Image</label>
										  <input  name="file" type="file"  >
										  <p>
										     Dimensions: 400 x 300 (Max:4MB)<br>
										    JPG is the recommended format.
										  </p>   

										</div>


										<div class="form-group">
											<h3 style="display: inline;">Meta Information</h3>
											<p style="display: inline;">This information is needed for google and other search engines</p>
										</div>
										<div class="form-group" style="margin-left: 0;">
											<label for="video_link">Video Link:</label>
											<input type="text" class="form-control" id="video_link" name="video_link" value="{{ old('video_link') }}">
										</div>

										<div class="form-group" style="margin-left: 0;">
										  <label for="meta_title">Title:</label>
										  <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{ old('meta_title') }}">
										</div>

										<div class="form-group" style="margin-left: 0;">
											<label for="meta_desc">Description:</label>
											<input type="text" class="form-control" id="meta_desc" name="meta_desc" value="{{ old('meta_desc') }}">
										</div>

										<div class="form-group" style="margin-left: 0;">
											<label for="meta_keyword">Keyword:</label>
											<input type="text" class="form-control" id="meta_keyword" name="meta_keyword" value="{{ old('meta_keyword') }}">
										</div>

										<button type="submit" class="btn btn-success">Save</button>

									</form>

								</div>
							</div>

						</div><!-- .box-body ENDs -->

					</div><!-- .box ENDs -->

				</div><!-- col-lg-12 col-md-12 ENDs -->
			</div><!-- .row ENDs -->

		</section><!-- content section ENDs -->

	</div><!-- Content Wrapper. ENDs -->


@stop
