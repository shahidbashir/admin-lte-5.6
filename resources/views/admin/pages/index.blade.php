@extends('layouts.default')

@section('title')
	Pages List
	@parent
@stop


@section('content')

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Main content -->
		<section class="content">

			<div class="row">
				<div class="col-lg-12 col-md-12">

					<div class="box">
						<div class="box-header">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<h3 class="box-title">Pages</h3>
									<p class="m-t-10 m-b-10">
										<a href="{{route('admin.pages.create')}}" class="btn btn-success">Add Page</a>
									</p>
								</div>
							</div>
						</div><!-- .box-header ENDs -->


						<div class="box-body">
							{{-- @include('shared.errors') --}}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<table class="table table-hover table-stripped">
										<thead>
											<tr>
												<th>Title</th>
												<th>Meta Title</th>
												<th>Action</th>
											</tr>
										</thead>

										<tbody>
											@if ($pages)
												@foreach ($pages as $key => $page)
													<tr data-delete="{{ route('admin.pages.destroy',$page)}}">
														<td>{{ $page->title }}</td>
														<td>{{ $page->meta_title }}</td>
														<td>
															<a href="{{ route('admin.pages.edit',$page)}}"><i class="fa fa-pencil"></i></a>
															<a data-delete-trigger href="#"><i class="fa fa-trash"></i></a>
														</td>
													</tr>
												@endforeach
											@endif
										</tbody>
									</table>

								</div>
							</div>

						</div><!-- .box-body ENDs -->

					</div><!-- .box ENDs -->

				</div><!-- col-lg-12 col-md-12 ENDs -->
			</div><!-- .row ENDs -->

		</section><!-- content section ENDs -->

	</div><!-- Content Wrapper. ENDs -->


@stop
