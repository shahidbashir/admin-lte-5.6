@extends('layouts.default')

@section('title')
	Dashboard
	@parent
@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Dashboard</h3>
					</div>



					{{--<div class="box-body">--}}

						{{--@include('shared.errors')--}}

						{{--<div class="row">--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}

								{{--<div class="form-group">--}}
									{{--<label for="">Select 2 with search</label>--}}
									{{--<select name="" id="input" class="form-control select2"   required="required">--}}
										{{--<option value="">Select</option>--}}
										{{--<option value="1">One</option>--}}
										{{--<option value="2">Two</option>--}}
									{{--</select>--}}

								{{--</div>--}}

							{{--</div>--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}

								{{--<div class="form-group">--}}
									{{--<label for="">Multi Select</label>--}}
									{{--<select name="" id="input" class="form-control select2" multiple  required="required">--}}
										{{--<option value="">Select</option>--}}
										{{--<option value="1">One</option>--}}
										{{--<option value="2">Two</option>--}}
									{{--</select>--}}
								{{--</div>--}}

							{{--</div>--}}

						{{--</div>--}}


						{{--<div class="row">--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}

								{{--<div class="form-group">--}}
									{{--<label for="">Date Picker</label>--}}
									{{--<input type="text" class="form-control" data-datepicker id="" placeholder="">--}}
								{{--</div>--}}

							{{--</div>--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}

								{{--<div class="form-group">--}}
									{{--<label for="">Date Range Picker</label>--}}
									{{--<input type="text" class="form-control" data-daterangepicker id="" placeholder="">--}}
								{{--</div>--}}

							{{--</div>--}}

						{{--</div>--}}




						{{--<div class="row">--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}

								{{--<div class="form-group">--}}
									{{--<label for="">Wysiwyg Editor</label>--}}
									{{--<textarea name="content" class="mceEditor form-control">Sample text</textarea>--}}
								{{--</div>--}}

							{{--</div>--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}



							{{--</div>--}}

						{{--</div>--}}



						{{--<h4 class="bold">Helper Functions</h4>--}}
						{{--<hr>--}}
						{{--<div class="row">--}}
							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}
								{{--<p>To Get Logged in user name.</p>--}}
								{{--<pre>Auth::user()-&gt;name </pre>--}}
								{{--<p>To Get site name form dynmic config.</p>--}}
								{{--<pre>config('general.site_name', 'Laravel Systems') </pre>--}}

							{{--</div>--}}

							{{--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">--}}
								{{--<p>To Send Email use below script.</p>--}}
								{{--<pre>// Create array of data <br> $emailData = [ <br>  'action' =&gt; 'email', // view , email <br>  'template' =&gt; 'admin-details', <br>  'subject' =&gt; 'Login details', <br>  'toName' =&gt; $user-&gt;name, <br>  'to' =&gt; $user-&gt;email, <br>  'emailContent' =&gt; [ <br>  'user' =&gt; $user, ] <br>];<br> // send email <br>sendEmail($emailData);</pre>--}}

							{{--</div>--}}

						{{--</div>--}}




					{{--</div>--}}
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>

	</section>
</div>

@stop
