@extends('front_end.layouts.app')
@section('title') Reset Password — Snowflake Club @stop

@section('content')
    @include('front_end.layouts.partials.top-menu'/*,
     ['heading'=>"If you can't make the trip",
     'img'=>asset("front-end/images/cp-banner.jpg")]*/
     )
    <main id="main">
        <div class="container">

            <div class="top-btm-gutter9">

                @include('shared.errors')


                <form method="post" action="{{ route('password.email') }}">


                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-offset-3 col-md-offset-4 col-sm-6 col-md-4">

                                <h4><b>Reset Password</b></h4>


                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" name="email"  class="form-control" placeholder="Email" value="{{ old('email') }}">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
                                @endif
                            </div>
                            {!! app('captcha')->display($attributes=[]) !!}

                            <br>
                                <button type="submit" class="btn btn-style2"> Send Password Reset Link</button>

                        </div>
                    </div>
                </form>

            </div>
        </div>
    </main>

    <!-- /.login-box -->

@stop
@section('js')

@stop

