@extends('front_end.layouts.app')
@section('title') Reset Password — Snowflake Club @stop

@section('content')
    @include('front_end.layouts.partials.top-menu'/*,
     ['heading'=>"If you can't make the trip",
     'img'=>asset("front-end/images/cp-banner.jpg")]*/
     )
    <main id="main">
        <div class="container">

            <div class="top-btm-gutter9">

                @include('shared.errors')

                <form method="post" action="{{ url('password/reset') }}">
                    <div class="row">
                        <div class="col-sm-offset-3 col-md-offset-4 col-sm-6 col-md-4">


                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" name="email" class="form-control" placeholder="Email"
                                       alue="{{ $email or old('email') }}" autofocus>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input type="password" name="password_confirmation" class="form-control"
                                       placeholder="Confirm password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
              <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
                                @endif
                            </div>


                            <button type="submit" class="btn btn-style2"> Reset Password
                            </button>

                            <!-- /.col -->
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </main>

    <!-- /.login-box -->

@stop
@section('js')

@stop

