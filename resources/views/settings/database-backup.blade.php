@extends('layouts.default')

@section('title')
	Database Backup Settings
	@parent
@stop



@section('content')

	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box">



				
				<div class="box-body">

					@if (count($errors) > 0)
					<div class="alert special alert-danger">
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					    
					      @foreach($errors->getMessages() as $errors)
					    	@foreach ($errors as $error)
					    		{{$error}}<br>
					    	@endforeach
					    @endforeach
					    
					</div>

					@endif

					<div class="callout callout-info">
		                <p>Database backup will be save in given dropbox account inside site name folder.</p>
		              </div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							
						</div>
					</div> <!-- row -->
					
					<div class="row">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

							<br>
							<p>
								<a href="{{url('/')}}/create-backup" class="btn btn-success">Create New Backup</a>
							</p>
							<br>

							
							<?php $options  = array( 'daily' => "Daily", 'weekly' => "Weekly", 'monthly' => "Monthly" ); ?>

							<p>
								Schedule Auto Backup 
							</p>
							<form action="{{url('/')}}/save-db-backup-setting" method="POST" role="form">
								<input type="hidden" name="_token" id="input" class="form-control" value="{{ csrf_token() }}">
								<div class="form-group">
									<label for="">Select Schedule</label>
									<select name="schedule" id="input" class="form-control" required="required">
										<option value="" >Select</option>
										@foreach ($options as $key => $vlaue )
											<option value="{{$key}}" <?php echo $retVal = ( isset($backupsettings)  && $backupsettings->schedule==$key) ? 'selected' : '' ; ?>>{{$vlaue}}</option>
										@endforeach
									</select>
								</div>

								<div class="checkbox">
									
									<label> 
										<input name="email" type="checkbox" value="1" <?php echo $retVal = (isset($backupsettings)  && $backupsettings->email=='1') ? 'checked' : '' ; ?>>
											Send email to admin of each backup.
									</label>
								</div>

								<div class="checkbox">
									
									<label> 
										<input name="attachment" type="checkbox" value="1" <?php echo $retVal = (isset($backupsettings)  && $backupsettings->attachment=='1') ? 'checked' : '' ; ?>>
											Send email with out attachment.
									</label>
								</div>


								
							
								
								
								<button type="submit" class="btn btn-success">Save</button>


							</form>
							<br>
							
							
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<br><br>
							<p>Dropbox API Setting</p>
							<form action="{{url('/')}}/save-dropbox-setting"  method="POST" role="form">
								<input type="hidden" name="_token" id="input" class="form-control" value="{{ csrf_token() }}">
								
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for="">Secret Key</label>
											<input name="dropbox_api" type="text" class="form-control" id="" placeholder="" value="{{ $retVal = ( isset($backupsettings) ) ? $backupsettings->dropbox_api : '' }}">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for="">Token</label>
											<input name="dropbox_token"   type="text" class="form-control" id="" value="{{$retVal = ( isset($backupsettings) ) ? $backupsettings->dropbox_token: ""}}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										
										<div class="form-group">
											<label for="">Dropbox Folder Name</label>
											<input name="folder_name" type="text" class="form-control" id="" placeholder="" value="{{ $backupsettings->folder_name or '' }}">
											<p class="help-block">Leave blank if you don't have directions for a specific folder name.</p>
										</div>
										
									</div>
								</div>
								
							
								
							
								<button type="submit" class="btn btn-success">Save</button>
							</form>
						</div>
					</div>
					
					<br><br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p style="font-weight:bold;">Database Backup History ( Last five backups )</p>

								<table class="table table-responsive table-bordered table-striped">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Backup Date</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										@if ( !$history->isEmpty() )
										<?php $i = 1;  ?>
											@foreach ($history as $record)
												<tr>
													<td>{{$i}}</td>
													<td>{{date('m/d/Y  H:s',strtotime($record->created_at))}}</td>
													<td>
														@if ($record->status == '1')
															<i class="fa fa-check" style="font-size:16px; color: green; "
															data-toggle="popover" title="Backup created successfully" data-content=""></i>
														@else
															<i class="fa fa-close" style="font-size:16px; color: red; "
															data-toggle="popover" title="Error Creating Backup" data-content=""></i>
														@endif
													</td>
												</tr>
												<?php $i = $i+1;  ?>
											@endforeach
										@else
										<tr>
											<td colspan="3" style="text-align: center; ">No backup have been taken yet.</td>
										</tr>
										@endif
										

										

									</tbody>
								</table>
						</div>
					</div>

				</div>
				
				
		</div>
	</div>
	
		
	
	

	

	</div>
	<!-- ./row -->
	<style type="text/css">
	.table-bordered>thead>tr>th, {
			border: 1px solid #ddd;
	}
	.table-condensed thead>tr>th {
				border-bottom: 1px solid #ddd;
		}
		.table-condensed tbody>tr>td {
				border-bottom: 1px solid #ddd;
		}
	</style>
	<script>
	$(document).ready(function(){
	    $('[data-toggle="popover"]').popover(); 
	});
	</script>
@stop
