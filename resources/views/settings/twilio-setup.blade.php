@extends('layouts.default')

@section('title')
	Twilio Settings
	@parent
@stop


@section('content')

	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box">



				
				<div class="box-body">

					@if (count($errors) > 0)
					<div class="alert special alert-danger">
					    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					    
					      @foreach($errors->getMessages() as $errors)
					    	@foreach ($errors as $error)
					    		{{$error}}<br>
					    	@endforeach
					    @endforeach
					    
					</div>

					@endif

					

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							
						</div>
					</div> <!-- row -->
					
					<div class="row">
						<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
							
							<form action="{{url('/')}}/twilio-setup" method="POST" role="form">
									<input type="hidden" name="_token" id="input" class="form-control" value="{{ csrf_token() }}">

								
								<div class="form-group">
									<label for="">SID</label>
									<input name="sid" type="text" class="form-control" id="" placeholder=""
									value="{{ $retVal = ( isset($twiliosettings) ) ? $twiliosettings->sid : '' }}">
								</div>

								<div class="form-group">
									<label for="">Token</label>
									<input name="token" type="text" class="form-control" id="" placeholder=""
									value="{{ $retVal = ( isset($twiliosettings) ) ? $twiliosettings->token : '' }}">
								</div>

								<div class="form-group">
									<label for="">From Number</label>
									<input name="from" type="text" class="form-control" id="" placeholder=""
									value="{{ $retVal = ( isset($twiliosettings) ) ? $twiliosettings->from : '' }}">
									<p class="help-block">Number format should be like this +15053955372</p>
								</div>
									
								<br>
								<button type="submit" class="btn btn-success">Save</button>

							</form>
							
							
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
						
					</div>
					<br>
					<p>
						SMS History From Twilio
					</p>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							
							@if (!empty($messages)  ) 
	 							<table class="table table-responsive table-bordered table-striped">
	 							<thead>
	 								<tr>
	 									<th>From</th>
	 									<th>To</th>
	 									<th>Date</th>
	 									<th>Body</th>
	 									<th>Status</th>
	 								</tr>
	 							</thead>
	 							<tbody>
									@foreach ($messages as $record )
											<?php if ($record['status'] =='received') {
												continue; 
											} ?>
										
											<tr>
												<td>{{$record['from']}}</td>
												<td>{{$record['to']}}</td>
												<td>{{$record['date']}}</td>
												<td>{!!$record['body']!!}</td>
												<td>{{ucfirst($record['status'])}}</td>

											</tr>
										
										
									@endforeach
								</tbody>
								</table>

								@else
									<p>{{$error}}</p>
								@endif
						</div>
					</div>
					
					<br><br>
					

				</div>
				
				
		</div>
	</div>
	
		
	
	

	

	</div>
	<!-- ./row -->
	<style type="text/css">
	.table-bordered>thead>tr>th, {
			border: 1px solid #ddd;
	}
	.table-condensed thead>tr>th {
				border-bottom: 1px solid #ddd;
		}
		.table-condensed tbody>tr>td {
				border-bottom: 1px solid #ddd;
		}
	</style>
	<script>
	$(document).ready(function(){
	    $('[data-toggle="popover"]').popover(); 
	});
	</script>
@stop
