@extends('layouts.default')
@section('title',  'Site Settings')
@section('styles')
    <style>
        input {
            margin-bottom: 18px !important;
        }
    </style>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('admin.settings.store.site') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="box">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                                    </div>
                                </div>
                            </div> <!-- box-header -->

                            <div class="box-body">

                                {{-- Errors and messages --}}
                                @include('shared.errors')
                                {{-- // Errors and messages --}}

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <input id="enable_sales_tax_hidden" type="hidden"
                                                           name="config[enable_sales_tax]" value="0"/>
                                                    <input type="checkbox" name="config[enable_sales_tax]" value="1"
                                                           id="enable_sales_tax"
                                                           @if(@$config->enable_sales_tax == true) checked
                                                           @endif value="1">
                                                    Enable Sales Tax
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="form-group" id="sales_tax_price_div">
                                                    <label><strong>Sales Tax %</strong></label><br>
                                                    <input type="text" class="form-control" id="sales_tax_price"
                                                           @if(@$config->enable_sales_tax != true) readonly
                                                           @endif placeholder="0"
                                                           style="max-width: 80px"
                                                           name="config[sales_tax_price]"
                                                           value="{{isset($config->sales_tax_price)?number_format($config->sales_tax_price,2):'0' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <input id="" type="hidden"
                                                           name="config[add_setting_option]" value="0"/>
                                                    <input type="checkbox" name="config[add_setting_option]" value="1"
                                                           id="add_setting_option"
                                                           @if(@$config->add_setting_option == true) checked
                                                           @endif value="1">
                                                            Setting Options
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- row -->

                            </div><!-- /.box-body -->
                        </div>
                        <p class="m-t-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </p>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('javascript')
    <script>
        $('document').ready(function () {

            @if(@$config->enable_sales_tax != true)
            $('#sales_tax_price_div').hide();
            @endif
                    
            $('#enable_sales_tax').change(function () {
                if (this.checked && $(this).val() == 1) {
                    $('#sales_tax_price_div').show();
                } else {
                    $('#sales_tax_price_div').hide();
                }
            });

        });
    </script>
@endsection