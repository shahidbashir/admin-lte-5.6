@extends('layouts.default')

@section('title')
	Gateway Settings
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <style>
            .dn{
                display: none;
            }
        </style>
@stop

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-5 col-md-5 margin-bottom">
                <form action="{{ route('admin.settings.gateway') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Select Payment Gateway</h3>
                                </div>
                                <div class="box-body">
                                    @include('shared.errors')
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group {{ $errors->has('config.gateway') ? 'has-error' : '' }}">
                                                <?php 
                                                $gateway = old('config.gateway') ? old('config.gateway') : config('gateway.gateway');
                                                ?>

                                                <label for="input_name">Select Gateway</label>
                                                <select name="config[gateway]" id="input_name" class="form-control select2" select-gateway>
                                                    <option value="" disabled>Select</option>
                                                    <option value="stripe" @if($gateway =='stripe') selected @endif>Stripe</option>
                                                    <option value="authorize" @if($gateway =='authorize') selected @endif>Authorize.net</option>
                                                    <option value="braintree" @if($gateway =='braintree') selected @endif>Braintree</option>
                                                </select>

                                                @foreach($errors->get('config.gateway') as $message)
                                                <span class="help-block text-danger">{{ $message }}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group {{ $errors->has('config.mode') ? 'has-error' : '' }}">
                                                
                                                <?php $mode = old('config.mode') ? old('config.mode') : config('gateway.mode') ?>
                                                <div class="form-group">
                                                    <label for="mode">Gateway Mode</label>
                                                    <select name="config[mode]" id="mode" class="form-control select2" >
                                                        <option value="" disabled>Select</option>
                                                        <option value="demo" @if($mode == 'demo') selected @endif>Demo</option>
                                                        <option value="live" @if($mode == 'live') selected @endif>Live</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group {{ $errors->has('config.method') ? 'has-error' : '' }}">

                                                <?php $method = old('config.method') ? old('config.method') : config('gateway.method') ?>
                                                <div class="form-group hide">
                                                    <label for="method">Payment Method</label>
                                                    <select name="config[method]" id="method" class="form-control select2" >
                                                        <option value="" disabled>Select</option>
                                                        <option value="charge" @if($method == 'charge') selected @endif>Charge Card</option>
                                                        <option value="create_profile" @if($method == 'create_profile') selected @endif>Create Customer Profile</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div stripe-setting style="display:none;">

                                                    <div class="form-group {{ $errors->has('config.stripe.secret') ? 'has-error' : '' }}">
                                                        <label for="key1">Secret</label>
                                                        <input type="text" class="form-control" id="key1" name="config[stripe][secret]"
                                                               value="{{ old('config.stripe.secret') ? old('config.stripe.secret') : config('gateway.stripe.secret') }}">
                                                    </div>


                                                    <div class="form-group {{ $errors->has('config.stripe.publish') ? 'has-error' : '' }}">
                                                        <label for="key2">Publish</label>
                                                        <input type="text" class="form-control" id="key2" name="config[stripe][publish]"
                                                               value="{{ old('config.stripe.publish') ? old('config.stripe.publish') : config('gateway.stripe.publish') }}">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div braintree-setting style="display:none;">

                                                    <div class="form-group {{ $errors->has('config.braintree.private_key') ? 'has-error' : '' }}">
                                                        <label for="key1">Private Key</label>
                                                        <input type="text" class="form-control" id="key1" name="config[braintree][private_key]"
                                                               value="{{ old('config.braintree.private_key') ? old('config.braintree.private_key') : config('gateway.braintree.private_key') }}">
                                                    </div>

                                                    <div class="form-group {{ $errors->has('config.braintree.public_key') ? 'has-error' : '' }}">
                                                        <label for="key1">Public Key</label>
                                                        <input type="text" class="form-control" id="key1" name="config[braintree][public_key]"
                                                               value="{{ old('config.braintree.public_key') ? old('config.braintree.public_key') : config('gateway.braintree.public_key') }}">
                                                    </div>

                                                    <div class="form-group {{ $errors->has('config.braintree.mechant_id') ? 'has-error' : '' }}">
                                                        <label for="key1">Mechant Key</label>
                                                        <input type="text" class="form-control" id="key1" name="config[braintree][mechant_id]"
                                                               value="{{ old('config.braintree.mechant_id') ? old('config.braintree.mechant_id') : config('gateway.braintree.mechant_id') }}">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div authorize-setting style="display:none;">

                                                    <div class="form-group {{ $errors->has('config.authorize.login') ? 'has-error' : '' }}">
                                                        <label for="key1">Login</label>
                                                        <input type="text" class="form-control" id="key1" name="config[authorize][login]"
                                                               value="{{ old('config.authorize.login') ? old('config.authorize.login') : config('gateway.authorize.login') }}">
                                                    </div>

                                                    <div class="form-group {{ $errors->has('config.authorize.key') ? 'has-error' : '' }}">
                                                        <label for="key1">Key </label>
                                                        <input type="text" class="form-control" id="key1" name="config[authorize][key]"
                                                               value="{{ old('config.authorize.key') ? old('config.authorize.key') : config('gateway.authorize.key') }}">
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./row -->
                    <button type="submit" class="btn btn-primary">Update Settings</button>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop


@section('javascript')
  <script>

     $('[{{$gateway}}-setting]').show();
      
     $('[select-gateway]').change(function(){
       var selectedOption = $(this).find(':selected').val();

       if(selectedOption == 'stripe'){
         $('[stripe-setting]').show();
         $('[braintree-setting]').hide();
         $('[authorize-setting]').hide();
       }

        if (selectedOption == 'authorize'){
          $('[authorize-setting]').show();
         $('[stripe-setting]').hide();
         $('[braintree-setting]').hide();
       }

        if (selectedOption == 'braintree'){
         $('[braintree-setting]').show();
         $('[stripe-setting]').hide();
         $('[authorize-setting]').hide();
       }

     });
     $('[select-gateway]').trigger('change');
   </script>

@stop