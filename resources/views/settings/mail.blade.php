@extends('layouts.default')

@section('title')
	Mail Settings
	@parent
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <style>
            .dn{
                display: none;
            }
        </style>
@stop

@section('javascript')
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
  


  <script>
    $('[data-driver]').change(function(){
      var selectedOption = $(this).find(':selected').val();

      if(selectedOption == 'smtp'){
        $('[data-smtp]').show();
        $('[data-mailgun]').hide();
      } else {
        $('[data-smtp]').hide();
        $('[data-mailgun]').show();
      }
    });
  </script>

    
@stop

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
          <div class="col-lg-5 col-md-5 margin-bottom">
            <form action="{{ route('admin.settings.mail', $setting) }}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <div class="col-xs-12">

                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">{{ $setting->title }}
                      <small>{{ $setting->description }}</small>
                    </h3>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('config.driver') ? 'has-error' : '' }}">
                          <?php $driver = old('config.driver') ? old('config.driver') : config('mail.driver') ?>
                          <label class="field-required">Driver</label>
                          <select name="config[driver]" class="form-control" data-driver>
                            <option selected disabled>Select mail driver</option>
                            <option value="smtp" @if($driver == 'smtp') selected @endif>SMTP</option>
                            <option value="mailgun" @if($driver == 'mailgun') selected @endif>Mailgun (API)</option>
                            <option value="log" @if($driver == 'log') selected @endif>Log Developer Testing</option>
                          </select>

                          @foreach($errors->get('config.driver') as $message)
                            <span class="help-block text-danger">{{ $message }}</span>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- ./row -->
            <div class="row">
              <div class="col-xs-12" data-revealed>

                  @include('settings.partials.__mailgun')

                  @include('settings.partials.__smtp')

              </div>
            </div>

            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                  </div>
                  <div class="box-body">
                    <div class="row">
                      {{--</div>--}}
                      <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('config.from.address') ? 'has-error' : '' }}">
                          <label for="site-url" class="field-required">From (email)</label>
                          <input name="config[from][address]" id="site-url"
                               type="text" class="form-control" placeholder="somecompany{{'@'}}somewhere.com"
                               value="{{ old('config.from.address') ? old('config.from.address') : config('mail.from.address') }}">
                          @foreach($errors->get('config.from.address') as $message)
                            <span class="help-block text-danger">{{ $message }}</span>
                          @endforeach
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('config.from.name') ? 'has-error' : '' }}">
                          <label for="site-url" class="field-required">From (name)</label>
                          <input name="config[from][name]" id="site-url"
                               type="text" class="form-control" placeholder="SomeCompany"
                               value="{{ old('config.from.name') ? old('config.from.name') : config('mail.from.name') }}">
                          @foreach($errors->get('config.from.name') as $message)
                            <span class="help-block text-danger">{{ $message }}</span>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <button type="submit" class="btn btn-primary">Update Settings</button>
        </form>
    </div>
          <div class="col-lg-7 col-md-7">
            <!-- ./row -->
            <div class="row">
              <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Email Management
                              <small></small>
                          </h3>
                      </div>
                      <div class="box-body">
                          <form action="{{ route('admin.settings.send-email') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                  <div class="col-xs-6">
                                    <div class="form-group {{ $errors->has('to') ? 'has-error' : '' }}">
                                      <label for="site-url" class="field-required">To (email)</label>
                                      <input name="to" id="site-url"
                                           type="email" class="form-control" placeholder="someone{{'@'}}somewhere.com"
                                           value="{{ old('to') }}"
                                      >
                                      @foreach($errors->get('to') as $message)
                                        <span class="help-block text-danger">{{ $message }}</span>
                                      @endforeach
                                    </div>
                                  </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                                    <label for="message" class="control-label">Message</label>
                                                    <textarea name="message" cols="30" rows="5" placeholder="Message" class="form-control mceEditor">
                                                        {{ old('message') }}
                                                    </textarea>
                                                    @foreach($errors->get('message') as $message)
                                                            <span class="help-block">{{ $message }}</span>
                                                    @endforeach
                                            </div>
                                    </div>
                            </div>
                            <button type="submit" class="btn btn-primary margin-bottom">Send</button>
                          </form>
                            <h3 class="box-title hide">Email History</h3>
                            <table class="table table-hover hide">
                                    <tr>
                                            <th>S.No</th>
                                            <th>To</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                    </tr>

                                    @foreach($emails as $index => $email)
                                    <tr>
                                            <td>{{ $index+1 }}</td>
                                            <td>
                                                {{ $email->to }}
                                            </td>
                                            <td>{{date('m/d/Y  H:s',strtotime($email->created_at))}}</td>
                                            <td>
                                                @if(empty($email->status))
                                                    <span class="label label-danger">Failed</span>
                                                @else
                                                    <span class="label label-success">Sent</span>
                                                @endif
                                            </td>
                                            <td><span class="btn btn-primary btn-xs" data-toggle="modal" data-target="#view{{ $email->id }}">View</span></td>
                                    </tr>
                                    @endforeach
                            </table>
                      </div>
                  </div>
              </div>
            </div>
          </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
  
@foreach($emails as $index => $email)
<!-- Modal -->
    <div id="view{{ $email->id }}" class="modal fade dn" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{ $email->subject }}</h4>
          </div>
          <div class="modal-body">
            {!! strip_tags($email->view) !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
@stop


@section('javascript')
<script>
  $('[data-driver]').change(function(){
    var selectedOption = $(this).find(':selected').val();

    if(selectedOption == 'smtp'){
      $('[data-smtp]').show();
      $('[data-mailgun]').hide();
    } else {
      $('[data-smtp]').hide();
      $('[data-mailgun]').show();
    }
  });
</script>
@stop

@section('css')

@stop

