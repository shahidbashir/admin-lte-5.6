@extends('layouts.default')
@section('title',  'Twilio Settings')
@section('styles')
    <link rel="stylesheet" href="{{ asset('/theme-assets/plugins/datatables/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3 class="box-title">Twilio Settings</h3>
                                    <p class="m-t-10">
                                        <a href="#" class="btn btn-default">Back to List</a>
                                    </p>
                                </div>

                            </div>
                        </div> <!-- box-header -->

                        <div class="box-body">

                            {{-- Errors and messages --}}
                            @include('shared.errors')
                            {{-- // Errors and messages --}}

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <form action="{{ route('admin.settings.store.twilio') }}" method="POST" role="form">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="config[sid]">SID</label>
                                            {{--{{dd($twilioSetting)}}--}}
                                            <input name="config[sid]" type="text" class="form-control" id="config[sid]"
                                                   placeholder=""
                                                   value="{{ ( isset($twilioSetting) ) ? @$twilioSetting->sid : '' }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="config[token]">Token</label>
                                            <input name="config[token]" type="text" class="form-control"
                                                   id="config[token]" placeholder=""
                                                   value="{{ ( isset($twilioSetting) ) ? @$twilioSetting->token : '' }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="config[from]">From Number</label>
                                            <input name="config[from]" type="text" class="form-control"
                                                   id="config[from]" placeholder=""
                                                   value="{{ ( isset($twilioSetting) ) ? @$twilioSetting->from : '' }}">
                                            <p class="help-block">Number format should be like this +15053955372</p>
                                        </div>

                                        <button type="submit" class="btn btn-primary m-t-20">Update</button>

                                    </form>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div>

                    
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('/theme-assets/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/theme-assets/plugins/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(function () {
            $("#sms_table").DataTable();
        });
    </script>
@endsection