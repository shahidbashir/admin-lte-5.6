<!-- spacer-lg -->
  <table class="email_table" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
    <tbody>
      <tr>
        <td class="email_body tc" style="box-sizing: border-box;vertical-align: top;line-height: 100%;text-align: center;padding-left: 16px;padding-right: 16px;background-color: #2A84B2;font-size: 0 !important;">
          <!--[if (mso)|(IE)]><table width="632" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:632px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
          <div class="email_container" style="box-sizing: border-box;font-size: 0;display: inline-block;width: 100%;vertical-align: top;max-width: 632px;margin: 0 auto;text-align: center;line-height: inherit;min-width: 0 !important;">
            <table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
              <tbody>
                <tr>
                  <td class="content_cell" style="box-sizing: border-box;vertical-align: top;width: 100%;background-color: #ffffff;font-size: 0;text-align: center;padding-left: 16px;padding-right: 16px;line-height: inherit;min-width: 0 !important;">
                    <table class="hr_rl" align="center" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 0;line-height: 1px;mso-line-height-rule: exactly;min-height: 1px;overflow: hidden;height: 2px;background-color: transparent !important;">
                      <tbody>
                        <tr>
                          <td class="hr_ep pte" style="box-sizing: border-box;vertical-align: top;font-size: 0;line-height: inherit;mso-line-height-rule: exactly;min-height: 1px;overflow: hidden;height: 2px;padding-top: 32px;background-color: transparent !important;">&nbsp; </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
        </td>
      </tr>
    </tbody>
  </table>
  
<!-- footer_default -->
  <table class="email_table" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
    <tbody>
      <tr>
        <td class="email_body email_end tc" style="box-sizing: border-box;vertical-align: top;line-height: 100%;text-align: center;padding-left: 16px;padding-right: 16px;padding-bottom: 32px;background-color: #2A84B2;font-size: 0 !important;">
          <!--[if (mso)|(IE)]><table width="632" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:632px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
          <div class="email_container" style="box-sizing: border-box;font-size: 0;display: inline-block;width: 100%;vertical-align: top;max-width: 632px;margin: 0 auto;text-align: center;line-height: inherit;min-width: 0 !important;">
            <table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
              <tbody>
                <tr>
                  <td class="content_cell footer_c default_b brb pt pb" style="box-sizing: border-box;vertical-align: top;width: 100%;background-color: #1F1F1F;font-size: 0;text-align: center;padding-left: 16px;padding-right: 16px;border-radius: 0 0 4px 4px;padding-top: 16px;padding-bottom: 16px;line-height: inherit;min-width: 0 !important;">
                    <!-- col-2-4 -->
                    <div class="email_row" style="box-sizing: border-box;font-size: 0;display: block;width: 100%;vertical-align: top;margin: 0 auto;text-align: center;clear: both;line-height: inherit;min-width: 0 !important;max-width: 600px !important;">
                    <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_4" style="box-sizing: border-box;font-size: 0;display: inline-block;width: 100%;vertical-align: top;max-width: 600px;line-height: inherit;min-width: 0 !important;">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_xs pb_0 tl sc" style="box-sizing: border-box;vertical-align: top;width: 100%;min-width: 100%;padding-top: 8px;padding-bottom: 0;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 23px;color: #ffffff;mso-line-height-rule: exactly;text-align: left;padding-left: 16px;padding-right: 16px;">
                                <p class="mb_xxs" style="font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 23px;color: #ffffff;mso-line-height-rule: exactly;display: block;margin-top: 0;margin-bottom: 4px;">
                                   ©  <?php echo date('Y') ?>  {{ config('general.site_name') }} <br>
                                  <small style="font-size: 14px;">SNOWFLAKE CLUB, 1672 OLD SKOKIE VALLEY ROAD, HIGHLAND PARK, IL, 60035, UNITED  STATES</small>
                                  <small> (847) 831-4300 SNOWFLAKE@WILLIAMSSKIANDPATIO.COM</small>
                                </p>
                                <p class="small mb_0" style="font-family: Helvetica, Arial, sans-serif;font-size: 14px;line-height: 20px;color: #ffffff;mso-line-height-rule: exactly;display: block;margin-top: 0;margin-bottom: 0;"><a href="https://snowflakeclub.org/trip" style="text-decoration: underline;line-height: inherit;color: #ffffff;"><span style="text-decoration: underline;line-height: inherit;color: #ffffff;">Trips</span></a> <span style="line-height: inherit;">&nbsp; • &nbsp;</span> <a href="https://snowflakeclub.org/faq" style="text-decoration: underline;line-height: inherit;color: #ffffff;"><span style="text-decoration: underline;line-height: inherit;color: #ffffff;">Faqs</span></a> 
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    <!--[if (mso)|(IE)]></td><td width="200" style="width:200px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_2" style="box-sizing: border-box;font-size: 0;display: inline-block;width: 100%;vertical-align: top;max-width: 200px;line-height: inherit;min-width: 0 !important;">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;min-width: 100%;">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_xs pb_0 ord_cell tr sc" style="box-sizing: border-box;vertical-align: top;width: 100%;min-width: 100%;padding-top: 8px;padding-bottom: 0;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 23px;color: #ffffff;mso-line-height-rule: exactly;text-align: right;padding-left: 16px;padding-right: 16px;">
                                <p class="fsocial mb_0" style="font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 100%;color: #b7b2c1;mso-line-height-rule: exactly;display: block;margin-top: 0;margin-bottom: 0;"><a href="#" style="text-decoration: underline;line-height: inherit;color: #ffffff;"><img src="{{url('/')}}/assets/images/email/social-facebook_white.png" width="24" height="24" alt="" style="outline: none;border: 0;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;line-height: 100%;max-width: 24px;height: auto !important;"></a> &nbsp;&nbsp; <a href="#" style="text-decoration: underline;line-height: inherit;color: #ffffff;"><img src="{{url('/')}}/assets/images/email/social-twitter_white.png" width="24" height="24" alt="" style="outline: none;border: 0;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;line-height: 100%;max-width: 24px;height: auto !important;"></a> &nbsp;&nbsp; <a href="#" style="text-decoration: underline;line-height: inherit;color: #ffffff;"><img src="{{url('/')}}/assets/images/email/social-instagram_white.png" width="24" height="24" alt="" style="outline: none;border: 0;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;line-height: 100%;max-width: 24px;height: auto !important;"></a></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
                  </div></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
