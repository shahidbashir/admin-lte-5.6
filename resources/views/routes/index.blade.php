@extends('layouts.default')
@section('title')
    Routes
    @parent
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12 col-lg-12">
                    <div class="box">

                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3 class="box-title">Routes</h3>
                                    <p class="m-t-20">
                                         <a href="{{ route('routes.create') }}" class="btn btn-success">Add Route
                                            </a>
                                    </p>
                                </div>

                            </div>
                        </div> <!-- box-header -->

                        <div class="box-body">

                            @include('shared.errors')

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <table class="table table-hover normal-table">
                                        <thead>
                                            <tr>
                                                <th>Route Title</th>
                                                <th>Locations</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($routes as $route)
                                            <tr data-delete="{{ route('routes.destroy', $route) }}">
                                                <td>{{ $route->name }}</td>
                                                <td>
                                                    @foreach($route->Locations as $point)
                                                        <span  class="label label-default">{{$point->title . '  '. $point->departs_time .' to '. $point->return_time}}<br/></span>
                                                    @endforeach

                                                </td>
                                                <td class="text-center">
                                                    <a href="{{ route('routes.show', $route) }}"><i
                                                                class="fa fa-eye"></i></a>
                                                    <a href="{{ route('routes.edit', $route) }}"><i
                                                                class="fa fa-pencil"></i></a>
                                                    <a href="#" data-delete-trigger><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4" class="text-center">No data found</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>

                                </div>
                            </div> <!-- row -->
                            {{ $routes->render() }}
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
