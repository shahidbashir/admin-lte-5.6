@extends('layouts.default')
@section('title')
    Show Route
    @parent
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12 col-lg-12">

                    <div class="box">

                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3 class="box-title">Show Route</h3>
                                    <p class="m-t-10">
                                        <a href="{{ route('routes.index') }}" class="btn btn-default">Back to List</a>
                                    </p>
                                </div>

                            </div>
                        </div> <!-- box-header -->

                        <div class="box-body">

                            @include('shared.errors')
                            <div class="row">
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">

                                    @include('routes.partials._form', [
                                        'url' => route('routes.update', @$route),
                                        'route' => @$route,
                                        'show' => true,
                                    ])

                                </div>
                            </div> <!-- row -->

                        </div><!-- /.box-body -->
                    </div>

                </div><!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
