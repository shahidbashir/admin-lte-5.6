<form action="{{route('routes.location.update',$id)}}" method="POST" role="form" id="edit_location_form" class="m-b-20 location-stop-edit">

    {{ csrf_field() }}
    <input type="text" name="id" id="location_id" value="{{$location['id']}}">

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" name="title" value="{{$location['title']}}" class="form-control"
                       id="edit_title_location" placeholder="">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <label for="">Map Link</label>
                <input type="text" name="map_link" value="{{$location['map_link'] }}"
                       class="form-control" id="edit_map_link" placeholder="">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="">Departs Time</label>
                <input type="text" name="departs_time" value="{{$location['departs_time'] }}"
                       class="form-control timepick" id="edit_departs_time" placeholder="00:00 AM">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="">Return Time: </label>
                <input type="text" name="return_time" value="{{$location['return_time'] }}"
                       class="form-control timepick" id="edit_return_time" placeholder="00:00 AM">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="">Max # of Customers</label>
                <input type="text" name="number_of_customer" value="{{ $location['number_of_customer'] }}"
                       class="form-control" id="edit_num_customer" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="">Address</label>
                <input type="text" name="address" value="{{ $location['address'] }}"
                       class="form-control" id="edit_address_id" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="">Description</label>
                <textarea name="desc" id="edit_desc_id" class="form-control"
                          value="{{ $location['desc'] }}" rows="3">{{ $location['desc'] }}</textarea>
            </div>
        </div>
    </div>

    <button type="submit" id="update_locations" class="btn btn-success">Update</button>
</form>

