<table class="table table-bordered m-t-20">
    <thead>
    <tr>
        <th>Name</th>
        <th>Departs Time</th>
        <th>Return Time</th>
        <th>max # of Customer</th>
        @if(!isset($show))

        <th class="text-center">Action</th>
        @endif

    </tr>
    </thead>
    <tbody>
    @if(session()->get('locations'))
        @forelse(session()->get('locations') as $key=>$value)
            <tr>
                <td>{{$value['title']}}</td>
                <td>{{$value['departs_time']}}</td>
                <td>{{$value['return_time']}}</td>
                <td>{{$value['number_of_customer']}}</td>
                @if(!isset($show))
                    <td class="text-center">
                        <a data-toggle="modal" href='#edit-location-stop' class="edit_locations" data-url="{{route('routes.location.edit',$key)}}"><i class="fa fa-edit"></i> </a>
                            <a class="remove_locations" data-url="{{route('routes.location.remove',$key)}}"><i class="fa fa-trash"></i> </a>
                    </td>
                @endif

            </tr>
        @empty

            <p>no data fund</p>

        @endforelse

    @endif

    </tbody>
</table>
