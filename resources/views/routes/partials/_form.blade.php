<form action="{{ $url }}" method="POST" role="form" class="m-b-20">

    {{ csrf_field() }}

    @if(isset($route))
        <input type="hidden" name="_method" value="put">
    @endif


    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="name">Route Name</label>
                <input type="text" name="name" class="form-control"
                       value="{{ isset($route) ? $route->name : old('name') }}" id="name" @isset($show) disabled
                       @endisset placeholder="" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="form-group">
                <label for="description">Short Description</label>
                <input type="text" name="description" class="form-control"
                       value="{{ isset($route) ? $route->description : old('description') }}" id="description"
                       @isset($show) disabled
                       @endisset placeholder="" required>
            </div>
        </div>
    </div>


    @if(!isset($show))

        <div class="location-stop well ">
            <div id="error_div"></div>
            <h3 class="m-t-20 m-b-20">Location</h3>

            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="title" value="{{old('title')}}" class="form-control"
                               id="title_location" placeholder="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="map_link">Map Link</label>
                        <input type="text" name="map_link" value="{{old('map_link') }}"
                               class="form-control" id="map_link" placeholder="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="departs_time">Departs Time</label>
                        <input type="text" name="departs_time" value="{{old('departs_time') }}"
                               class="form-control timepick" id="departs_time" placeholder="00:00 AM">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="return_time">Return Time: </label>
                        <input type="text" name="return_time" value="{{old('return_time') }}"
                               class="form-control timepick" id="return_time" placeholder="00:00 AM">
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="address_id">Address</label>
                        <input type="text" name="address" value="{{ old('address') }}"
                               class="form-control" id="address_id" placeholder="">
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="num_customer">Max # of Customers</label>
                        <input type="text" name="number_of_customer" value="{{ old('number_of_customer') }}"
                               class="form-control" id="num_customer" placeholder="">
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="desc_id">Description</label>
                        <textarea name="desc" id="desc_id" class="form-control"
                                  value="{{ old('desc') }}" rows="3">{{ old('desc') }}</textarea>
                    </div>
                </div>
            </div>

            <button type="button" id="add_locations" class="btn btn-success">Add</button>

        </div>
    @endif


    <h4 class="m-t-20">Locations</h4>
    <div id="results">
        @include('routes.partials.locations_table')
    </div>


    <div class="row m-t-10">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="">Status</label>
                <select name="status" id="input" class="form-control" @isset($show) disabled
                        @endisset required="required">
                    <option value="">Select</option>
                    @if(isset($route) && $route->status)
                        <option {{$route->status=='1'?'selected':''}} value="1">Active</option>
                        <option {{$route->status=='0'?'selected':''}}  value="0">Inactive</option>
                    @else
                        <option {{@old('status')=='1'?'selected':''}} value="1">Active</option>
                        <option {{@old('status')=='0'?'selected':''}}  value="0">Inactive</option>
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="display_order">Display Order</label>
                <input type="text" class="form-control" id="display_order"
                       value="{{ isset($route) ? $route->display_order : old('display_order') }}"
                       name="display_order" @isset($show) disabled @endisset placeholder="" style="width: 90px;">
            </div>
        </div>
    </div>


    @if(!isset($show))

        <button type="submit"
                class="btn btn-primary m-t-10">{{ isset($route) ? 'Edit Route' : 'Add Route' }}</button>
    @endif
</form>
