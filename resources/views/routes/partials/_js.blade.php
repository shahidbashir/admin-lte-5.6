<script>
    $(document).ready(function () {
        $(document).on("click", "#add_locations", function () {

            var title = $('#title_location').val();
            var map_link    = $('#map_link').val();
            var departs_time = $('#departs_time').val();
            var return_time = $('#return_time').val();
            var number_of_customer = $('#num_customer').val();
            var address_id = $('#address_id').val();
            var desc_id = $('#desc_id').val();
            var url = '{{route('routes.location')}}';
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    title: title,
                    map_link: map_link,
                    departs_time: departs_time,
                    return_time: return_time,
                    address: address_id,
                    desc: desc_id,
                    number_of_customer: number_of_customer,
                },
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function ($data) {
                    $('.location-stop #title_location').val('');
                    $('.location-stop  #map_link').val('');
                    $('.location-stop  #address_id').val('');
                    $('.location-stop  #num_customer').val('');
                    $('.location-stop  #desc_id').val('').html('');
                    $('.location-stop  #departs_time').val('').trigger('change');
                    $('.location-stop  #return_time').val('').trigger('change');

                    $("#results").html('').append($data);
                    $( '#error_div' ).html('');

                },
                error:function(response){
                    if( response.status === 422 ) {
                        //process validation errors here.
                        $errors = response.responseJSON; //this will get the errors response data.
                        errorsHtml = '<div class="alert alert-danger"><ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul></div>';

                        $( '#error_div' ).html( errorsHtml );
                    }
                }
            });
        });
        $(document).on("click", ".remove_locations", function () {

            var url = $(this).attr('data-url');
            console.log(url);
            $.ajax({
                url: url,
                type: 'GET',
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function ($data) {

                    $("#results").html('').append($data);
                }

            });
        });
        $(document).on("click", ".edit_locations", function () {

            var url = $(this).attr('data-url');
            console.log(url);
            $.ajax({
                url: url,
                type: 'GET',
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function ($data) {

                    $("#edit-location-stop-results").html('').append($data);
                    timepick();
                    updateLocation();
                }

            });
        });
        updateLocation();
    });
    function updateLocation() {


        $('#edit_location_form').on('submit', function (e) {
            e.preventDefault();
            var id = $('#location_id').val();
            var title = $('#edit_title_location').val();
            var map_link    = $('#edit_map_link').val();
            var departs_time = $('#edit_departs_time').val();
            var return_time = $('#edit_return_time').val();
            var number_of_customer = $('#edit_num_customer').val();
            var address_id = $('#edit_address_id').val();
            var desc_id = $('#edit_desc_id').val();
            var url = $(this).attr('action');
            $('#edit-location-stop').modal('hide');

            $.ajax({
                url: url,
                type: 'POST',
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                data: {
                    id:id,
                    title: title,
                    map_link: map_link,
                    departs_time: departs_time,
                    return_time: return_time,
                    address: address_id,
                    desc: desc_id,
                    number_of_customer: number_of_customer,
                },
                success: function ($data) {
                    $("#results").html('').append($data);
                    $('#error_div').html('');
                },
                error: function (response) {
                    if( response.status === 422 ) {
                        //process validation errors here.
                        $errors = response.responseJSON; //this will get the errors response data.
                        errorsHtml = '<div class="alert alert-danger"><ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul></div>';

                        $( '#error_div' ).html( errorsHtml );
                    }
                }
            });

        });

    }


</script>
