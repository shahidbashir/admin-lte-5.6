@extends('layouts.default')
@section('title')
    Edit Route
    @parent
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12 col-lg-12">

                    <div class="box">

                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3 class="box-title">Edit Route</h3>
                                    <p class="m-t-10">
                                        <a href="{{ route('routes.index') }}" class="btn btn-default">Back to List</a>
                                    </p>
                                </div>

                            </div>
                        </div> <!-- box-header -->

                        <div class="box-body">

                            @include('shared.errors')

                            <div class="row">
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">

                                    @include('routes.partials._form', [
                                        'url' => route('routes.update', $route),
                                        'route' => $route,
                                    ])

                                </div>
                            </div> <!-- row -->

                        </div><!-- /.box-body -->
                    </div>

                </div><!-- /.box-body -->
            </div>
        </section>
    </div>
    <div class="modal fade" id="edit-location-stop">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Location Stop</h4>
                </div>
                <div id="edit-location-stop-results" style="padding: 25px">
{{--                    @include('busstop_page.partials._edit_location')--}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    @include('routes.partials._js')
@endsection