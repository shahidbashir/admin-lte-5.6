@extends('layouts.default')
@section('title',  'Email Template Listing')
@section('styles')
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        <div class="box-header">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h3 class="box-title">Email Template Listing</h3>
                                    <p class="m-t-10">
                                        {{--<a href="{{url('email_templates/create')}}" class="btn btn-success">Add Template</a>--}}
                                    </p>
                                </div>

                            </div>
                        </div> <!-- box-header -->

                        <div class="box-body">

                            {{-- Errors and messages --}}
                            @include('shared.errors')
                            {{-- // Errors and messages --}}

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <table class="table table-responsive table-bordered table-striped">
                                        <tr>
                                            <th>Title</th>
                                            <th>Subject</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        @if(count($email_templates)>0)
                                            @foreach($email_templates as $i)
                                                <tr data-delete="{{ route('email_templates.destroy', $i) }}">

                                                    <td>{{$i->title}}</td>
                                                    <td>{{$i->subject}}</td>

                                                    <td class="text-center">
                                                        <a href="{{route('email_templates.edit', $i->id)}}"
                                                           class="fa fa-pencil"></a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No results found in Email Templates</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div> <!-- row -->

                        </div><!-- /.box-body -->
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')

    <script>

        jQuery(document).ready(function ($) {
            $('#form-submit').on('click', function (event) {
                event.preventDefault();
                $('form').submit();
            });
        });

    </script>

@endsection