<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail, DB;
use App\Http\Requests;
use App\EmailHisotry;

class EmailController extends Controller
{

    public static function getAdminName()
    {
       return   config('mail.from.name');
    }

    public static function getAdminEmail()
    {
       return  config('mail.from.address');
    }


    public static function emailPreview( $emailData )
    {

         return view('emails.'. $emailData['template'] )->with('emailData', $emailData );

    }

    public static function sendEmail( $emailData ) {

       

        $emailData['view'] = view('emails.'. $emailData['template'] )->with('emailData', $emailData );
       // $email = EmailHisotry::storeEmail($emailData);
        
        // test view
        if ( !empty($emailData['action']) && $emailData['action'] == 'view' ) {

            return view('emails.'. $emailData['template'] )->with('emailData', $emailData );
        }
        
        Mail::send('emails.'.$emailData['template'], ['emailData' => $emailData] , function ($message) use ($emailData) {

            $message->subject( $emailData['subject'] );
            $message->to( $emailData['to'] , (isset($emailData['toName']) ) ? $emailData['toName'] : '' );

            if( isset($emailData['cc']) && !empty($emailData['cc']) ) {
                $message->cc( $emailData['cc'] , $emailData['ccName'] );
            }

            if(isset($emailData['bcc']) && !empty($emailData['bcc']) ) {
                $message->cc( $emailData['bcc'] , $emailData['bccName'] );
            }
            if( !isset($emailData['from']) || empty( $emailData['fromName']) ) {

                $message->from( self::getAdminEmail() , self::getAdminName()  );

            } else {
                $message->from( $emailData['from']  , $emailData['fromName']  );

            }


        });
        
        if ( count( Mail::failures() ) > 0 ) {
            // $email->status = 0;
            // $email->save();
            return false;
        } else {
            // $email->status = 1;
            // $email->save();
            return true;
        }
    }


}
