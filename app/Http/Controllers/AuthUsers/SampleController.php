<?php

namespace App\Http\Controllers\AuthUsers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SampleController extends Controller
{
 	protected $viewsFolder = 'protected.';    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view($this->viewsFolder.'index');
    }
}
