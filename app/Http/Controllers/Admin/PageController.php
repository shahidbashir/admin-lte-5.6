<?php namespace App\Http\Controllers\Admin;

use Session;
use App\Models\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PageController extends Controller {

	public function index()
	{
		$pages = Page::orderBy('created_at', 'desc')->get();
		return view('admin.pages.index')
						->with('pages', $pages);

	}

	public function create()
	{	


		return view('admin.pages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
				'title' 			 => 'required|max:255',
				'content' 		 => 'required',
				'meta_title' 	 => 'required|max:255',
				'meta_desc'		 => 'required|max:255',
				'meta_keyword' => 'required|max:255',
		]);

		$page = new Page;
		$page->title 				= $request->title;
		$page->content 			= $request['content'];
		$page->meta_title 	= $request->meta_title;
		$page->meta_desc 		= $request->meta_desc;
		$page->meta_keyword = $request->meta_keyword;
		$page->video_link = $request->video_link;

		if ($request->hasFile('file')) { 

			$this->uploadfile($request, $request->file('file'),  $page, 'file');
		}

		$page->save();

		Session::flash('success', "New page added");
		return redirect('admin/pages');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::findOrFail($id);
		return view('admin.pages.edit')
						->with('page', $page);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
				'title' 			 => 'required|max:255',
				'content' 		 => 'required',
				'meta_title' 	 => 'required|max:255',
				'meta_desc'		 => 'required|max:255',
				'meta_keyword' => 'required|max:255',
		]);

		$page = Page::findOrFail($id);
		$page->title 				= $request->title;
		$page->content 			= $request['content'];
		$page->meta_title 	= $request->meta_title;
		$page->meta_desc 		= $request->meta_desc;
		$page->meta_keyword = $request->meta_keyword;
		$page->save();

		if ($request->hasFile('file')) { 

			 	$this->uploadfile($request, $request->file('file'),  $page, 'file');
			 }

		
	    Session::flash('success', "Page details updated");

		$page = Page::findOrFail($id);
		return redirect()->route('admin.pages.index');

		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Page::destroy($id);
		
		return response()->json('success');
	}


    public function uploadfile(Request $request, $image ,  $page, $filelocation )
    {
    		

            $file_name = str_random(6);
		     
		    $name = $file_name . '-' . rand(1, 6000) . '-' . $image->getClientOriginalName();
		   	
            $fileupload = $image->move( public_path() . '/uploads/images', $name);

            if ( $filelocation == 'file' ) {
            	$page->file = $name;
            }

            $page->save();

       
    }


}
