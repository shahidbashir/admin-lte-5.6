<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Log; 
use Session; 
use Cache;
class AdminController extends Controller
{
	


	public function index() {
	    $users = User::where('type', 'admin')->orwhere('type','staff')->orderBy('created_at', 'desc')->get();

	    return view('admin.users.index', [
	        'users' => $users
	    ]);
	}

	/**
	 * Update User
	 * URL: /admin/users/{user} (POST)
	 *
	 * @param Request $request
	 * @param $user
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id) {
	    $data = $request->all();
	    $user = User::find($id);
	    // Validation
	    $this->validate($request, [
	        'name' => 'required|min:2|max:255',
	        'email' => 'required|max:255|email|unique:users,email,' . $id,
	        'type' => 'required|in:user,admin'
	    ]);

	    foreach ([
	                 'name',
	                 'email',
	                 'type',
	             ] as $field) {
	        if (isset($data[$field]) && $data[$field] != $user->{$field}) {
	            $user->{$field} = $data[$field];
	        }
	    }

	    // Set the new password
	    if (!empty($data['password'])) {
	    	 // Validation
	        $this->validate($request, [
	            'password' => 'required|min:6|max:255|confirmed',
	        ]);

	        $user->{'password'} = bcrypt($data['password']);
	    }

	    $user->save();

	    return redirect(route('admin.users.index', $user['id']))
	        ->with('success', 'Your user information has been updated successfully.');
	}

	 public function edit($id)
    {   
        
        $user = User::where('id', $id)->first();
        
        return view('admin.users.edit')->with('user', $user);
    }

    


	 public function create()
    {   
        
        $user  = ''; 
        
        return view('admin.users.create')->with('user', $user);
    }


    public function store(Request $request) {
        $data = $request->all();

        // Validation
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required|max:255|email|unique:users,email',
            'password' => 'required|min:6|max:255|confirmed',
            'type' => 'required|in:user,admin,staff'
        ]);


	
      	$user =  User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'type' => $request['type'],
        ]); 

        if( !empty($request->get('email_details')) ){
        	
        	$emailData = [
        	    'action' => 'email', // view , email
        	    'template' => 'admin-details',
        	    'subject' => 'Login details',
        	    'toName' => $user->name,
        	    'to' => $user->email,
        	    'emailContent' => [
        	        'user' => $user,
        	         'password' => $request['password'],
        	    ]
        	];
        	// send
        	sendEmail($emailData);

        }

        
     	
       Session::flash('success', 'New '. ucfirst($user->type) .' user has been created successfully');

       return redirect(route('admin.users.index'));
    }

     public function destroy($id)
     {
     	
        // delete admin, only if he's not an emergency admin
		$user = User::where('id', $id)
			->where('email', '!=', env('EMERGENCY_ADMIN_EMAIL'))
			->first();

		$user->delete();

		Cache::forget('general');
		return response()->json('success',200);

    }

    
}
