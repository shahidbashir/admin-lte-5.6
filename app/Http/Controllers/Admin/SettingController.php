<?php namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Session,Cache;
use App\Setting;
use App\Http\Requests\SendEmailRequest;
use App\Http\Controllers\EmailController;
use App\EmailHisotry;
use DOMDocument;
use Twilio\Rest\Client;

class SettingController extends Controller {

   

    protected $viewsFolder = 'settings.';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function siteSetting()
    {
        $setting = Setting::query()->where('key', 'site')->first();
        $config = $setting ? (object) @$setting->config : (object) config('site');
        \Cache::forget('site');
        return view('settings.site', compact('setting', 'config'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSiteSetting(Request $request)
    {
        $setting = Setting::query()->firstOrNew([
            'key' => 'site',
            'view_name' => 'settings.site',
            'title' => 'Site Settings',
        ]);
//        dd($setting,$request->config);
        $setting->updateConfig($request->config);

        $request->session()->flash('success', 'Updated successfully!');
        return redirect(route('admin.settings.site'));
    }


    public function showGeneral()
    {
        $setting = Setting::where('key','general')->first();
       
        $config = $setting->config;

        Cache::forget('general');
        
        return view($this->viewsFolder.'general', compact('setting', 'config'));
    }


     public function updateGeneral(Request $request)
    {   
        $input = $request->all();
        
        $this->validateGenral($request);

        $setting = Setting::where('key','general')->first();
        $setting->updateConfig($input['config']); 

        Session::flash('success', "Settings were updated successfully");

        return back();
    }


	public function showMail()
	{
		$setting = Setting::where('key','mail')->first();
                $config = $setting->config;
                $emails = EmailHisotry::getRecent();
		return view($this->viewsFolder.'mail', compact('setting', 'config', 'emails'));
	}


    public function updateMail(Request $request)
    {   
        $input = $request->all();
        
        if ($input['config']['driver'] == 'mailgun') {
            $this->validateMailgun($request);
        }

        if ($input['config']['driver'] == 'smtp') {
           $this->validateSMTP($request);
        }

        $setting = Setting::where('key','mail')->first();
        $setting->updateConfig($input['config']); 

        Session::flash('success', "Settings were updated successfully");

        return back();
    }
	
    public function destroy_email($setting_id, $email = "")
    {
        $setting = Setting::find($setting_id);

        $success = $setting->removeEmail($email)->save();

        return response()->json(compact('success'));
    }

    public function validateMailgun($request)
    {
        
        $rules = [
            
            'config.from.name' => 'required',
            'config.from.address' => 'required|email',
            'config.secret' => 'required',
            'config.domain' => 'required',
        ]; 

        $this->validate($request,$rules); 

    }


    public function validateSMTP($request)
    {
        
        $rules = [
            'config.host' => 'required',
            'config.port' => 'required|numeric',
            'config.username' => 'required',
            'config.password' => 'required',
            'config.from.name' => 'required',
            'config.from.address' => 'required|email',
        ]; 

        $this->validate($request,$rules); 

    }


    public function validateGenral($request)
    {
        
        $rules = [
            'config.site_name' => 'required',
            'config.site_url' => 'required',
            'config.default_email' => 'required',
        ]; 

        $this->validate($request,$rules); 

    }
    
    public function sendEmail(SendEmailRequest $request)
    {   

        // Create array of data 
         $emailData = [ 
          'action' => 'email', // view , email 
          'template' => 'simple', 
          'subject' => 'Test Email',
          'to' => $request->get('to'), 
          'emailContent' => [ 
              'message' => $request->get('message'), 
              ]
        ];
        // send email 
        $sent = sendEmail($emailData);
        
        if($sent)
        {
            Session::flash('success', "Email sent successfully!");
            return back();
            
        } else {
            Session::flash('error', "Email sending failed!");
            return back()->withInput();
            
        }
    }
    
    public function showGateway($value='')
    {
        $setting = Setting::where('key','gateway')->first();
        $config = $setting->config;
        return view( 'settings.gateway', compact('setting', 'config'));
    }


    public function showGatewayAdmin($value='')
    {
        $setting = Setting::where('key','gateway')->first();

        $config = $setting->config;


        return view( 'settings.gateway-admin', compact('setting', 'config'));
    }


    public function updateGateway(Request $request)
    {
       $input = $request->all();

       $this->validateGateway($request);

       $setting = Setting::where('key','gateway')->first();
       
       $setting->updateConfig($input['config']);
       \Cache::forget('gateway');

       Session::flash('success', "Settings were updated successfully");

       return back();
    }


    public function validateGateway($request)
    {

        $rules = [
            'config.mode' => 'required|in:0,1',
            'config.mode' => 'required',
        ];

        $this->validate($request,$rules);

    }

    public function twilioSetting()
    {
        $messages = [];
        $error = null;

        $setting = Setting::query()->where('key', 'twilio')->first();


//        if(! $setting) {
//            return view('setting.twilio', compact('twilioSetting', 'messages', 'error'));
//        }

        $twilioSetting = $setting ? (object) @$setting->config : (object) config('twilio');

        // try {
        //     $client = new Client(@$twilioSetting->sid, @$twilioSetting->token);
        //     // Get Recent Calls
        //     foreach ($client->account->messages->read() as $key => $call) {

        //         $dateCreated = (array)$call->dateCreated;

        //         $messages[$key]['from'] = $call->from;
        //         $messages[$key]['to'] = $call->to;
        //         $messages[$key]['body'] = $call->body;
        //         $messages[$key]['date'] = date('m/d/Y  H:s', strtotime($dateCreated['date']));
        //         $messages[$key]['status'] = $call->status;

        //         //$time = $call->startTime->format("Y-m-d H:i:s");
        //         //echo "Call from $call->from to $call->to body: $call->body \n";
        //     }
        // } catch (\Exception $e) {
        //     $error  = $e->getMessage();
        // }

        return view('settings.twilio', compact('twilioSetting', 'messages', 'error'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTwilioSetting(Request $request)
    {
        $this->validate($request, [
            'config.sid' => 'required',
            'config.token' => 'required',
            'config.from' => 'required',
        ]);

        $setting = Setting::query()->firstOrNew([
            'key' => 'twilio',
            'view_name' => 'settings.twilio',
            'title' => 'Twilio Settings',
        ]);

        $setting->updateConfig($request->config);

        $request->session()->flash('success', 'Settings saved.');
        return back();
    }
    

}
