<?php
namespace App\Http\ViewComposers;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;

class AppComposer
{
	/**
	 * Retrieve and cache static pages
	 */
	protected function staticPages()
	{
		// return Cache::remember('pages_static', Carbon::now()->addMinutes(15), function() {
		// 	return \App\StaticPage::select(['id', 'title'])->where('title','!=','Home Page bottom section')->get();
		// });
	}

	/**
	 * Retrieve and cache settings pages
	 */
	protected function settingsPages()
	{
		// return Cache::remember('pages_settings', Carbon::now()->addMinutes(15), function() {
		// 	return \App\Setting::select(['id', 'key', 'title'])->get();
		// });
	}
	

	/**
	 * Retrieve and cache notifications
	 */
	

	

	/**
	 * Share variables with views
	 *
	 * @param View $view
	 */
	public function compose(View $view)
	{
		$view->with([
			'pages_static'              => $this->staticPages(),
			'pages_settings'            => $this->settingsPages(),
			
			
		]);
	}
}
