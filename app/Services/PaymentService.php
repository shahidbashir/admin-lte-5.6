<?php
namespace App\Services; 

use Illuminate\Http\Request;
use App\Services\StripeService;
use App\Services\AuthorizeService;
use App\Services\BraintreeService;
use App\Services\Interfaces\PaymentMethodAdapterInterface; 

class PaymentService implements PaymentMethodAdapterInterface
{

  protected $service;
  protected $createProfile;

  public function __construct($gateway = null , $createProfile = null)
  { 
    if ( null != $gateway ) {

       $this->service = $gateway; 

    } else {  

        if ( config('gateway.gateway' ) == 'stripe' ) {

          $this->service = new StripeService; 

        }

        if (config('gateway.gateway' ) == 'braintree' ){

          $this->service = new BraintreeService; 

        }

        if (config('gateway.gateway' ) == 'authorize') {

          $this->service = new AuthorizeService; 

        }
    }

    if ( null != $createProfile ) {
        
        $this->createProfile = true ;

    } elseif (config('gateway.method' ) == 'create_profile') {

      $this->createProfile = true ;
         
    } else {

         $this->createProfile = false ;
    } 
    
    
  }

  public function processPayment(Request $request )
  {
    
    if ( $this->createProfile == null ) {
      
      return $this->service->chargeCard($request);

    } else {

      return $this->service->createCustomerProfile($request);
      
    }

  }

  public function make_profile(Request $request )
  {
    
      return $this->service->createCustomerProfile($request);
    

  }

  public function chargeProfile(Request $request )
  {
    
      return $this->service->chargeProfile($request);
    

  }

  public function getTransactionList(Request $request )
  {
    
      return $this->service->getTransactionList($request);
    

  }
  public function refundTransaction(Request $request )
  {
    
      return $this->service->refundTransaction($request);
    

  }
  public function getTransactionDetail(Request $request )
  {
    
      return $this->service->getTransactionDetail($request);
    

  }

  
  


}
