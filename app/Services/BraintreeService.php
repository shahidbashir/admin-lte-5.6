<?php

namespace App\Services;

use Braintree\Customer;
use Braintree_Transaction;
use Braintree_CustomerSearch;
use Illuminate\Http\Request;
use App\Services\Interfaces\GatewayAdapterInterface;

class BraintreeService implements GatewayAdapterInterface
{



  public function chargeCard(Request $request)
  {
    $result = null;
    $last4  = null;
    $customerProfile  = null;

    $customer_profile_id = $this->getProfileByNounce( $request ); 

    $result = Braintree_Transaction::sale([
        'amount' => $request->amount,
        'customerId' => $customer_profile_id,
        'options' => [
         'submitForSettlement' => true,
        ]
    ]);

    $customerProfile = $result->transaction->id;
    $last4 = $result->transaction->creditCard['last4'];



    if($result->success === false) {
        $response = [
          'status' => 'error',
          'response' => $result->message,

        ];

        return (object)  $response ;
    }

    $response = [
      'status' => 'success',
      'customerProfile' => $customerProfile,
      'last4'           => $last4
    ];

    return (object) $response ;

  }

  public function createCustomerProfile( Request $request )
  {

      $result = null;
      $last4  = null;
      $customerProfile = null;

       $result = Customer::create([
        'firstName' => $request->first_name,
        'lastName'  => $request->last_name,
        'phone'     => $request->phone,
        'email'     => $request->email,
        'paymentMethodNonce' => $request->nonce,
      ]);


      $customerProfile = $result->customer->id;
      $last4 = $result->customer->paymentMethods[0]->last4;

      if($result->success === false) {
        $response = [
          'status' => 'error',
          'response' => $result->message,

          ];

          return (object)  $response ;
      }

      $response = [
        'status' => 'success',
        'customerProfile' => $customerProfile,
        'last4'           => $last4
      ];

      return (object) $response ;

  }


  public function getProfileByNounce( Request $request )
  {

      $result = null;
      $last4  = null;
      $customerProfile = null;

       $result = Customer::create([
        'firstName' => $request->first_name,
        'lastName'  => $request->last_name,
        'phone'     => $request->phone,
        'email'     => $request->email,
        'paymentMethodNonce' => $request->nonce,
      ]);

   
      
      $customerProfile = $result->customer->id;
      
      return  $customerProfile ;

  }


}
