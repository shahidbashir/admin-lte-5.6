<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 4/24/2018
 * Time: 3:20 PM
 */

namespace App\Services;


use App\Models\EmailTemplate;
use App\Models\Reservation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TwilioSmsService
{

    public function sendSMSTripMembers($trip, $message)
    {
        $msg = '';
        $members = $trip->members()->whereNotNull('phone')->get();
        foreach ($members as $member){
            $van_name = strtoupper($member->first_name.' '.$member->last_name);
            $msg  = 'Hello! '.$van_name.': '.$message;
            $this->sendSMS($member->phone, $msg);
        }

        $customer = $members->first()->customer;

        if(isset($customer) && $customer->metaUser->mobile_number)
        {
            $customer_name = strtoupper($customer->name);
            $msg  = 'Hello! '.$customer_name.': '.$message;
            $this->sendSMS($customer->metaUser->mobile_number, $msg);
        }
    }
    public function sendSMSTripLocationMembers($trip,$location_id, $message)
    {
        $msg = '';
        $members = $trip->members()->where('trip_member.location_id', $location_id)->whereNotNull('phone')->get();
        foreach ($members as $member){
            $van_name = strtoupper($member->first_name.' '.$member->last_name);
            $msg  = 'Hello! '.$van_name.': '.$message;
            $this->sendSMS($member->phone, $msg);
        }
        $customer = $members->first()->customer;

        if(isset($customer) && $customer->metaUser->mobile_number)
        {
            $customer_name = strtoupper($customer->name);
            $msg  = 'Hello! '.$customer_name.': '.$message;
            $this->sendSMS($customer->metaUser->mobile_number, $msg);
        }
    }
    /**
     * @param $phone
     * @param $msg
     * @return array
     */
    public function sendSMS($phone, $msg)
    {
        // format number required by twilio
        $to = '+1' . str_replace(['/', '(', ')', ' ', '-'], '', $phone);
        
        if ( env('TEST_TWILIO', false ) ) {

            $to = '+17274749392';

        } else {

            $to = '+1' . str_replace(['/', '(', ')', ' ', '-'], '', $phone);
            
        }

        

        // Your Account SID and Auth Token from twilio.com/console
        $client = new \Twilio\Rest\Client(config('twilio.sid'), config('twilio.token'));

        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
                $to, // the number you'd like to send the message to
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => config('twilio.from'),
                    // the body of the text message you'd like to send
                    'body' => $msg
                )
            );

            $status = true;
            $message = 'Message sent at: ' . \Carbon\Carbon::now() . ' to: ' . $to . ' message: ' . $msg;
            \Log::info($message);
        } catch (\Exception $e) {

            \Log::error('SMS Exception: '. $to  . $e->getMessage());
            $message = $e->getMessage();
            $status = false;
        }

        return array('status' => $status, 'message' => $message);
    }
}