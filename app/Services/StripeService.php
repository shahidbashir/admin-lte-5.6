<?php

namespace App\Services;


use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Http\Request;
use App\Services\Interfaces\GatewayAdapterInterface;

class StripeService  implements GatewayAdapterInterface
{ 

  private $getStripeSecretKey; 
  
  public function __construct(  )
  {
    $this->getStripeSecretKey = config('gateway.stripe.secret');

    Stripe::setApiKey( $this->getStripeSecretKey);

  }


  public function chargeCard( Request $request )
  {

      try {
        $charge = Charge::create(array(
          "amount" => $request->amount * 100,
          "currency" => "usd",
          "source" => $request->input('stripeToken'), // obtained with Stripe.js
          "description" => "Charged from " . $request->email,
        ));

      } catch (\Exception $e) {
        $response = [
          'status' => 'error',
          'error' => $e->getMessage(),

        ];

        return (object)  $response ;
      } // catch ends

      $response = [
        'status' => 'success',
        'customerProfile' => $charge->id,
        'last4'            => $charge->source->last4
      ];

      return (object) $response ;

    
  }

  public function createCustomerProfile( Request $request )
  {
      
      try {

        $charge = Customer::create(array(
          "email"  => $request->email,
          "source" => $request->input('stripeToken'), // obtained with Stripe.js
        ));

      } catch (\Exception $e) {

        $response = [
          'status' => 'error',
          'error' => $e,

        ];

        return (object)  $response ;
      }

      $response = [
        'status' => 'success',
        'customerProfile' => $charge->id,
        'last4'            => $charge->sources->data[0]->last4
      ];

      return (object) $response ;
  }



}
