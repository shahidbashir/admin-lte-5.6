<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

Interface GatewayAdapterInterface
{
  
  public function chargeCard(Request $request);
  
  public function createCustomerProfile(Request $request);
  
  public function chargeProfile(Request $request);
  public function getTransactionList(Request $request);
  public function refundTransaction(Request $request);
  public function getTransactionDetail(Request $request);
  
  
}
