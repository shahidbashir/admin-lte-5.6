<?php

namespace App;

use App\Models\CustomerChild;
use App\Models\CustomerEmail;
use App\Models\CustomerPaymentProfile;
use App\Models\MetaUser;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function metaUser()
    {
        return $this->hasOne(MetaUser::class,'user_id','id');
    }
    public function orders(){
        return $this->hasMany(Order::class,'customer_id');

    } 
    public function children(){
        return $this->hasMany(CustomerChild::class,'customer_id','id');
    }

    public function payment_profiles(){
        return $this->hasMany(CustomerPaymentProfile::class,'customer_id');
    }
    public function payments()
    {
        return $this->hasManyThrough(Payment::class, Order::class,'customer_id','membership_id');
    }
    public function totalMemberships()
    {
        return isset($this->children)?$this->children->count():0;
    }
    public function totalTripMember($trip_id)
    {
        $value = 0;
        foreach ($this->orders as $order)
        {
            $value = $value + $order->tripChildren()->where('trip_id',$trip_id)->count();
        }
        return  $value;
    }


    
    /**
     * The search by keyword.
     *
     * @string
     */

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere(function ($q) use ($keyword){
                        $q->whereHas('children',function ($q) use ($keyword){
                            $q->where("first_name", "LIKE","%$keyword%")
                            ->orWhere("last_name", "LIKE","%$keyword%");
                        });
                    });
            });
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emails()
    {
        return $this->hasMany(CustomerEmail::class, 'customer_id');
    }
    public function getNewTripMailVariables($trip)
    {
        return array(
            '[member]' => ucfirst($this->name),
            '[close_at]' => $trip->booking_close->format('m/d/Y H:i A' ),
            '[limit]' => $trip->limit,
            '[link]' => url('/'),
        );
    }
}
