<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MetaUser extends Model
{
    protected $guarded = [];
    protected $table ='meta_user';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
