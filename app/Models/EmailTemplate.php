<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    const NEW_MEMBER = 'New Member';
    const FAILED_TRANSACTION = 'Failed Transaction';
    const TRIP_PURCHASE = 'Trip Purchase';   
    const NEW_TRIP = 'New Trip Started';
    const REFUND_ISSUED = 'Refund Issued';//20
    const REFUND_FOR_EPIC_PASS = 'Refund for Epic Pass';//21
    const CANCELLATION_REFUND = 'Cancellation Refund';//22
    const CHARGE_FOR_PARENT_BUS_FEE = 'Charge for Parent Bus Fee';//23
    const CHARGE_FOR_LIFT_TICKET = 'Charge for Lift Ticket';//24
    const CHARGE_FOR_EQUIPMENT_RENTAL = 'Charge for Equipment Rental';//25


    protected $appends = ['variables', 'email_body'];


    public function scopeBySubject($query, $type)
    {
        return $query->where('subject', '=', trim($type));
    }

    public function createEmailBody($searchReplace)
    {
        $message = $this->replaceVariables($searchReplace);

        $message = $message . ' ' . $this->dynamic_body;

        $message = nl2br($message);
        $message = html_entity_decode(utf8_decode($message));

        return $message;
    }


    public function replaceVariables($searchReplace)
    {

        $message = $this->static_body;

        if (!config('site.enable_sales_tax')) {
            
            $message = str_replace('Sub Total:  [sub_total]','',$message ); 
            $message = str_replace('Sales Tax:  [sales_tax]','',$message ); 
        }

        foreach ($searchReplace as $key => $value) {

            $message = str_replace($key, $value, $message);
        }

        return $message;

    }

    /**
     * @return array
     */
    public function getVariablesAttribute()
    {
        if ($this->static_body) {

            preg_match_all('#\[(.*?)\]#', $this->static_body, $match);
            return $match[0];
        }

        return [];
    }

    /**
     * @return mixed|string
     */
    public function getEmailBody($searchReplace, $proposed_variables = null)
    {
        $this->email_body = $this->createEmailBody($searchReplace) . $proposed_variables;

        return $this;
    }

}
