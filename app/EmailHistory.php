<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailHisotry extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emails_history';
    public static $limit = 5;
    
    public static function getRecent()
    {
        return self::orderBy('id', 'desc')->limit(self::$limit)->get();
    }
    
    public static function storeEmail($emailData)
    {
        $email              = new EmailHisotry();
        $email->from        = empty($emailData['from'])?"":$emailData['from'];
        $email->to          = empty($emailData['to'])?"":$emailData['to'];
        $email->subject     = empty($emailData['subject'])?"":$emailData['subject'];
        $email->view        = empty($emailData['view'])?"":$emailData['view'];
        $email->status      = 0;
        $email->save();
        
        // delete all except recent 5
        self::where('id', '<=', $email->id - self::$limit)->orderBy('id', 'desc')->delete();
        return $email;
    }
}
